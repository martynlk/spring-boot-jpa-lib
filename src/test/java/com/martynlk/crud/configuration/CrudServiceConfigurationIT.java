package com.martynlk.crud.configuration;

import org.hibernate.validator.constraints.UniqueElements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrudServiceConfigurationIT.CrudServiceConfigurationItApplication.class)
public class CrudServiceConfigurationIT {

    @SpringBootApplication
    public static class CrudServiceConfigurationItApplication {
        public static void main(String[] args){
            SpringApplication.run(CrudServiceConfigurationItApplication.class, args);
        }

        @Bean
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("Foo");
        }
    }

    @Autowired private ValidatorFactory validatorFactory;
    private Validator validator;

    private CrudServiceConfiguration crudServiceConfiguration;

    @Before
    public void setup(){
        validator = validatorFactory.getValidator();
        crudServiceConfiguration = new CrudServiceConfiguration();
        crudServiceConfiguration.setCreateLimit(1);
        crudServiceConfiguration.setReadLimit(1);
        crudServiceConfiguration.setUpdateLimit(1);
        crudServiceConfiguration.setUpdateLimit(1);
        crudServiceConfiguration.setDeleteLimit(1);
        crudServiceConfiguration.setDefaultSortDirection(Sort.Direction.ASC);
        crudServiceConfiguration.setDefaultPropertiesToSortOn(Arrays.asList("created", "lastModified"));
    }

    @Test
    public void validate_configurationAsSetup_noViolationsOccur(){

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_createLimitIsSetTo0_violationOccurs(){

        // Given
        crudServiceConfiguration.setCreateLimit(0);

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(Min.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("createLimit", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_readLimitIsSetTo0_violationOccurs(){

        // Given
        crudServiceConfiguration.setReadLimit(0);

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(Min.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("readLimit", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_updateLimitIsSetTo0_violationOccurs(){

        // Given
        crudServiceConfiguration.setUpdateLimit(0);

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(Min.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("updateLimit", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_deleteLimitIsSetTo0_violationOccurs(){

        // Given
        crudServiceConfiguration.setDeleteLimit(0);

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(Min.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("deleteLimit", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_defaultSortDirectionIsSetToNull_violationOccurs(){

        // Given
        crudServiceConfiguration.setDefaultSortDirection(null);

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("defaultSortDirection", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_defaultSortDirectionIsSetToEverySortDirection_noViolationOccurs(){

        // Given
        Arrays.asList(Sort.Direction.values()).forEach((sortDirection) -> {
            crudServiceConfiguration.setDefaultSortDirection(sortDirection);

            // When
            Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

            // Assert
            assertEquals(0, violations.size());
        });
    }

    @Test
    public void validate_defaultPropertiesToSortOnIsEmpty_oneViolationOccurs(){

        // Given
        Arrays.asList(null, new ArrayList<String>()).forEach((propertiesToSortOn) -> {
            crudServiceConfiguration.setDefaultPropertiesToSortOn(propertiesToSortOn);

            // When
            Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
            assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("defaultPropertiesToSortOn", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void validate_defaultPropertiesToSortOnIsNotEmptyButContainsABlankString_oneViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankPropertyToSortOn) -> {
            crudServiceConfiguration.setDefaultPropertiesToSortOn(Arrays.asList("not blank", blankPropertyToSortOn));

            // When
            Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("defaultPropertiesToSortOn[1].<list element>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void validate_defaultPropertiesToSortOnIsNotEmptyButContainsDuplicateBlankStrings_oneViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankPropertyToSortOn) -> {
            crudServiceConfiguration.setDefaultPropertiesToSortOn(Arrays.asList(blankPropertyToSortOn, blankPropertyToSortOn));

            // When
            Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

            // Assert
            assertEquals(3, violations.size());

            List<ConstraintViolation<CrudServiceConfiguration>> sortedViolations = violations
                .stream()
                .sorted((violation1, violation2) -> {
                    String violation1AnnotationType = violation1.getPropertyPath().toString();
                    String violation2AnnotationType = violation2.getPropertyPath().toString();
                    return violation1AnnotationType.compareTo(violation2AnnotationType);
                })
                .sorted((violation1, violation2) -> {
                    String violation1AnnotationType = violation1.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName();
                    String violation2AnnotationType = violation2.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName();
                    return violation1AnnotationType.compareTo(violation2AnnotationType);
                })
                .collect(Collectors.toList());

            assertEquals(NotBlank.class, sortedViolations.get(0).getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("defaultPropertiesToSortOn[0].<list element>", sortedViolations.get(0).getPropertyPath().toString());

            assertEquals(NotBlank.class, sortedViolations.get(1).getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("defaultPropertiesToSortOn[1].<list element>", sortedViolations.get(1).getPropertyPath().toString());

            assertEquals(UniqueElements.class, sortedViolations.get(2).getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("defaultPropertiesToSortOn", sortedViolations.get(2).getPropertyPath().toString());
        });
    }

    @Test
    public void validate_defaultPropertiesToSortOnIsNotEmptyAndContainsNonBlankStringButThereAreDuplicates_oneViolationOccurs(){

        // Given
        crudServiceConfiguration.setDefaultPropertiesToSortOn(Arrays.asList("not blank 1", "not blank 1"));

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<CrudServiceConfiguration> violation = violations.iterator().next();
        assertEquals(UniqueElements.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("defaultPropertiesToSortOn", violation.getPropertyPath().toString());
    }

    @Test
    public void validate_defaultPropertiesToSortOnIsNotEmptyAndContainsNonBlankStringAndThereAreNoDuplicates_noViolationOccurs(){

        // Given
        crudServiceConfiguration.setDefaultPropertiesToSortOn(Arrays.asList("not blank 1", "not blank 2"));

        // When
        Set<ConstraintViolation<CrudServiceConfiguration>> violations = validator.validate(crudServiceConfiguration);

        // Assert
        assertEquals(0, violations.size());
    }
}
