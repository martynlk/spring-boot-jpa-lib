package com.martynlk.crud.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import static com.martynlk.crud.configuration.CrudLiquibaseConfiguration.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrudLiquibaseConfigurationIT {

    @Autowired private Environment environment;

    private static final String CRUD_LIQUIBASE_PROPERTIES_PREFIX = "spring.liquibase.parameters.";

    @Test
    public void crudLibraryLiquibaseProperties_standardLibrarySetup_liquibaseChangelogParameterValuesAreDeclaredInLibraryAsExpected(){
        assertEquals("id", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_NAME));
        assertEquals("uuid", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_TYPE));
        assertEquals("active", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_NAME));
        assertEquals("boolean", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_TYPE));
        assertEquals("created_by", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_NAME));
        assertEquals("varchar(255)", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_TYPE));
        assertEquals("created", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_NAME));
        assertEquals("datetime", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_TYPE));
        assertEquals("last_modified", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_NAME));
        assertEquals("datetime", environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_TYPE));
    }
}
