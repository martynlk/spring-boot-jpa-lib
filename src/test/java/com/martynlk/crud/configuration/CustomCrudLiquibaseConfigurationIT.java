package com.martynlk.crud.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import static com.martynlk.crud.configuration.CrudLiquibaseConfiguration.*;
import static com.martynlk.crud.configuration.CustomCrudLiquibaseConfigurationIT.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_NAME + "=" + CUSTOM_ID_COLUMN_NAME,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_TYPE + "=" + CUSTOM_ID_COLUMN_TYPE,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_NAME + "=" + CUSTOM_ACTIVE_COLUMN_NAME,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_TYPE + "=" + CUSTOM_ACTIVE_COLUMN_TYPE,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_NAME + "=" + CUSTOM_CREATED_BY_COLUMN_NAME,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_TYPE + "=" + CUSTOM_CREATED_BY_COLUMN_TYPE,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_NAME + "=" + CUSTOM_CREATED_COLUMN_NAME,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_TYPE + "=" + CUSTOM_CREATED_COLUMN_TYPE,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_NAME + "=" + CUSTOM_LAST_MODIFIED_COLUMN_NAME,
    CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_TYPE + "=" + CUSTOM_LAST_MODIFIED_COLUMN_TYPE

})
public class CustomCrudLiquibaseConfigurationIT {

    @Autowired
    private Environment environment;

    static final String CRUD_LIQUIBASE_PROPERTIES_PREFIX = "spring.liquibase.parameters.";
    static final String CUSTOM_ID_COLUMN_NAME = "foo";
    static final String CUSTOM_ID_COLUMN_TYPE = "bar";
    static final String CUSTOM_ACTIVE_COLUMN_NAME = "baz";
    static final String CUSTOM_ACTIVE_COLUMN_TYPE = "qux";
    static final String CUSTOM_CREATED_BY_COLUMN_NAME = "quux";
    static final String CUSTOM_CREATED_BY_COLUMN_TYPE = "corge";
    static final String CUSTOM_CREATED_COLUMN_NAME = "grault";
    static final String CUSTOM_CREATED_COLUMN_TYPE = "garply";
    static final String CUSTOM_LAST_MODIFIED_COLUMN_NAME = "waldo";
    static final String CUSTOM_LAST_MODIFIED_COLUMN_TYPE = "fred";

    @Test
    public void crudLibraryLiquibaseProperties_customSetup_changelogParameterValuesAreCustomValues(){
        assertEquals(CUSTOM_ID_COLUMN_NAME, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_NAME));
        assertEquals(CUSTOM_ID_COLUMN_TYPE, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ID_COLUMN_TYPE));
        assertEquals(CUSTOM_ACTIVE_COLUMN_NAME, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_NAME));
        assertEquals(CUSTOM_ACTIVE_COLUMN_TYPE, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + ACTIVE_COLUMN_TYPE));
        assertEquals(CUSTOM_CREATED_BY_COLUMN_NAME, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_NAME));
        assertEquals(CUSTOM_CREATED_BY_COLUMN_TYPE, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_BY_COLUMN_TYPE));
        assertEquals(CUSTOM_CREATED_COLUMN_NAME, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_NAME));
        assertEquals(CUSTOM_CREATED_COLUMN_TYPE, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + CREATED_COLUMN_TYPE));
        assertEquals(CUSTOM_LAST_MODIFIED_COLUMN_NAME, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_NAME));
        assertEquals(CUSTOM_LAST_MODIFIED_COLUMN_TYPE, environment.getProperty(CRUD_LIQUIBASE_PROPERTIES_PREFIX + LAST_MODIFIED_COLUMN_TYPE));
    }
}
