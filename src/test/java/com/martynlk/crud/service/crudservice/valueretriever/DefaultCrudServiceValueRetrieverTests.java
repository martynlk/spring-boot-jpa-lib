package com.martynlk.crud.service.crudservice.valueretriever;

import com.martynlk.crud.configuration.CrudServiceConfiguration;
import com.martynlk.crud.message.BaseMessages;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCrudServiceValueRetrieverTests {

    @Rule public ExpectedException exceptionRule = ExpectedException.none();

    @Mock private CrudServiceConfiguration crudServiceConfiguration;

    @InjectMocks
    private DefaultCrudServiceValueRetriever defaultCrudServiceValueRetriever;

    @Test
    public void getReadLimit_readLimitRequestedIsNull_readLimitInCrudServiceConfigurationReturned(){

        // Given
        BaseMessages baseMessages = mock(BaseMessages.class);

        int crudServiceConfigReadLimit = 2;
        given(crudServiceConfiguration.getReadLimit()).willReturn(crudServiceConfigReadLimit);

        Integer limitRequested = null;

        // When
        int result = defaultCrudServiceValueRetriever.getReadLimit(limitRequested, baseMessages);

        // Assert
        assertEquals(crudServiceConfigReadLimit, result);
    }

    @Test
    public void getReadLimit_readLimitRequestedIsNotNullAndIsLessThanTheReadLimitInCrudServiceConfiguration_readLimitRequestedReturned(){

        // Given
        BaseMessages baseMessages = mock(BaseMessages.class);

        int crudServiceConfigReadLimit = 2;
        given(crudServiceConfiguration.getReadLimit()).willReturn(crudServiceConfigReadLimit);

        int limitRequested = crudServiceConfigReadLimit - 1;

        // When
        int result = defaultCrudServiceValueRetriever.getReadLimit(limitRequested, baseMessages);

        // Assert
        assertEquals(limitRequested, result);
    }

    @Test
    public void getReadLimit_readLimitRequestedIsNotNullAndIsEqualToTheReadLimitInCrudServiceConfiguration_readLimitRequestedReturned(){

        // Given
        BaseMessages baseMessages = mock(BaseMessages.class);

        int crudServiceConfigReadLimit = 2;
        given(crudServiceConfiguration.getReadLimit()).willReturn(crudServiceConfigReadLimit);

        int limitRequested = crudServiceConfigReadLimit;

        // When
        int result = defaultCrudServiceValueRetriever.getReadLimit(limitRequested, baseMessages);

        // Assert
        assertEquals(limitRequested, result);
    }

    @Test
    public void getReadLimit_readLimitRequestedIsNotNullButIsGreaterThanTheReadLimitInCrudServiceConfiguration_exceptionThrown(){

        // Given
        BaseMessages baseMessages = mock(BaseMessages.class);

        int crudServiceConfigReadLimit = 2;
        given(crudServiceConfiguration.getReadLimit()).willReturn(crudServiceConfigReadLimit);

        int limitRequested = crudServiceConfigReadLimit + 1;

        String exceptionMessage = "foo";
        given(baseMessages.readLimitExceeded(anyInt(), anyInt())).willReturn(exceptionMessage);

        // Expect
        exceptionRule.expect(InvalidParameterException.class);
        exceptionRule.expectMessage(exceptionMessage);

        // When
        defaultCrudServiceValueRetriever.getReadLimit(limitRequested, baseMessages);
    }

    @Test
    public void getPage_pageRequestedIsNull_1IsReturned(){

        // Given
        Integer pageRequested = null;

        // When
        int result = defaultCrudServiceValueRetriever.getPage(pageRequested);

        // Assert
        assertEquals(1, result);
    }

    @Test
    public void getPage_pageRequestedIsNotNull_pageRequestedIsReturned(){

        // Given
        int pageRequested = 3000;

        // When
        int result = defaultCrudServiceValueRetriever.getPage(pageRequested);

        // Assert
        assertEquals(pageRequested, result);
    }

    @Test
    public void getSortDirectionToUse_sortDirectionIsBlankButDefaultSortDirectionFromCrudServiceConfigurationIsARecognisedSortDirection_defaultSortDirectionFromCrudServiceConfigurationReturned(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((sortDirectionRequested) -> {
            Arrays.asList(Sort.Direction.values()).forEach((crudServiceConfigurationSortDirection) -> {
                given(crudServiceConfiguration.getDefaultSortDirection()).willReturn(crudServiceConfigurationSortDirection);

                // When
                Sort.Direction result = defaultCrudServiceValueRetriever.getSortDirectionToUse(sortDirectionRequested);

                // Assert
                assertEquals(crudServiceConfigurationSortDirection, result);
            });
        });
    }

    @Test
    public void getSortDirectionToUse_sortDirectionIsNotBlankButIsNotARecognisedSortDirection_exceptionThrown(){

        // Given
        String sortDirectionRequested = "not blank, but not a valid sort direction";

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);

        // When
        defaultCrudServiceValueRetriever.getSortDirectionToUse(sortDirectionRequested);
    }

    @Test
    public void getSortDirectionToUse_sortDirectionIsNotBlankAndIsARecognisedSortDirection_sortDirectionRequestedReturned(){

        // Given
        Arrays.asList(Sort.Direction.values()).forEach((sortDirectionRequested) -> {

            // When
            Sort.Direction result = defaultCrudServiceValueRetriever.getSortDirectionToUse(sortDirectionRequested.name());

            // Assert
            assertEquals(sortDirectionRequested, result);
        });
    }

    @Test
    public void getPropertiesToSortOn_propertiesToSortOnIsEmpty_crudServiceConfigurationDefaultPropertiesToSortOnReturned(){

        // Given
        List<String> defaultPropertiesToSortOn = Arrays.asList("property 1", "property 2");
        given(crudServiceConfiguration.getDefaultPropertiesToSortOn()).willReturn(defaultPropertiesToSortOn);

        Arrays.asList(null, new ArrayList<String>()).forEach((propertiesRequestedToSortOn) -> {

            // When
            List<String> result = defaultCrudServiceValueRetriever.getPropertiesToSortOn(propertiesRequestedToSortOn);

            // Assert
            assertEquals(defaultPropertiesToSortOn, result);
        });
    }

    @Test
    public void getPropertiesToSortOn_propertiesRequestedToSortOnIsNotEmpty_propertiesRequestedToSortOnIsReturned(){

        // Given
        List<String> propertiesRequestedToSortOn = Arrays.asList("property 1", "property 2");

        // When
        List<String> result = defaultCrudServiceValueRetriever.getPropertiesToSortOn(propertiesRequestedToSortOn);

        // Assert
        // Assert
        assertEquals(propertiesRequestedToSortOn, result);
    }
}
