package com.martynlk.crud.service.crudservice;

import com.martynlk.crud.TestHelper;
import com.martynlk.crud.configuration.JpaAuditingConfiguration;
import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.entity.BaseEntity;
import com.martynlk.crud.mapper.BaseMapper;
import com.martynlk.crud.message.BaseMessages;
import com.martynlk.crud.service.crudservice.valueretriever.CrudServiceValueRetriever;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrudServiceValidationIT.CrudServiceTestValidationITApplication.class)
public class CrudServiceValidationIT {

    @SpringBootApplication
    public static class CrudServiceTestValidationITApplication {

        @Bean(JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME)
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("Foo");
        }
    }

    public class TestEntity extends BaseEntity {
        @Override public void update(@NotNull Object entityWithUpdates) { }
    }

    public class TestCrudService extends BaseCrudService {

        public TestCrudService(
            JpaRepository<TestEntity, UUID> baseJpaRepository,
            BaseMapper<InputDto, TestEntity, OutputDto> baseServiceMapper,
            BaseMessages baseMessages,
            CrudServiceValueRetriever crudServiceValueRetriever
        ) {
            super(
                baseJpaRepository,
                baseServiceMapper,
                baseMessages,
                crudServiceValueRetriever
            );
        }
    }

    @MockBean private JpaRepository<TestEntity, UUID> baseJpaRepositoryMock;
    @MockBean private BaseMapper<InputDto, TestEntity, OutputDto> baseServiceMapperMock;
    @MockBean private BaseMessages baseMessagesMock;
    @MockBean private CrudServiceValueRetriever crudServiceValueRetrieverMock;

    private TestCrudService testCrudService = new TestCrudService(
        baseJpaRepositoryMock,
        baseServiceMapperMock,
        baseMessagesMock,
        crudServiceValueRetrieverMock
    );

    @Autowired private ValidatorFactory validatorFactory;
    private ExecutableValidator executableValidator;
    private Method createMethod;
    private Method getAllMethod;
    private Method getOneMethod;
    private Method updateMethod;
    private Method deleteMethod;

    @Before
    public void setup(){
        executableValidator = validatorFactory.getValidator().forExecutables();

        try {
            createMethod = TestCrudService.class.getMethod("create", Set.class);
            getAllMethod = TestCrudService.class.getMethod("getAll", Integer.class, Integer.class, String.class, List.class);
            getOneMethod = TestCrudService.class.getMethod("getOne", String.class);
            updateMethod = TestCrudService.class.getMethod("update", Set.class);
            deleteMethod = TestCrudService.class.getMethod("delete", Set.class);
        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }
    }

    //////////////////////////
    ///// CREATE - INPUT /////
    //////////////////////////

    @Test
    public void create_inputDtosIsNull_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, createMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.arg0", violation.getPropertyPath().toString());
    }


    @Test
    public void create_inputDtosIsEmpty_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, createMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.arg0", violation.getPropertyPath().toString());
    }

    @Test
    public void create_inputDtosIsNotEmptyButHasNullElement_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, createMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.arg0[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void create_inputDtosIsNotEmptyAndHasNonNullElementButIsNotValid_constraintViolationOccurs(){

        // Given
        InputDto validInputDto = TestHelper.getValidInputDto();
        InputDto invalidInputDto = TestHelper.getInvalidInputDto();

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(validInputDto);
        inputDtos.add(invalidInputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, createMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.arg0[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void create_inputDtosIsNotEmptyAndHasNonNullElementsAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        InputDto validInputDto1 = TestHelper.getValidInputDto();
        InputDto validInputDto2 = TestHelper.getValidInputDto();

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(validInputDto1);
        inputDtos.add(validInputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, createMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(0, violations.size());
    }

    ///////////////////////////
    ///// CREATE - OUTPUT /////
    ///////////////////////////

    @Test
    public void create_outputDtosIsNull_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, createMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.<return value>", violation.getPropertyPath().toString());
    }


    @Test
    public void create_outputDtosIsEmpty_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, createMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.<return value>", violation.getPropertyPath().toString());
    }

    @Test
    public void create_outputDtosIsNotEmptyButHasNullElement_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, createMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.<return value>[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void create_outputDtosIsNotEmptyAndHasNonNullElementButIsNotValid_constraintViolationOccurs(){

        // Given
        OutputDto validOutputDto = TestHelper.getValidOutputDto();

        OutputDto invalidOutputDto = TestHelper.getInvalidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto);
        outputDtos.add(invalidOutputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, createMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("create.<return value>[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void create_outputDtosIsNotEmptyAndHasNonNullElementsAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        OutputDto validOutputDto1 = TestHelper.getValidOutputDto();
        OutputDto validOutputDto2 = TestHelper.getValidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto1);
        outputDtos.add(validOutputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, createMethod, outputDtos);

        // Assert
        assertEquals(0, violations.size());
    }

    ///////////////////////////
    ///// GET ALL - INPUT /////
    ///////////////////////////

    @Test
    public void getAll_allInputVariationsTested_constraintViolationsOccurUnderCertainConditions(){

        // Given
        List<Integer> requestedLimits = Arrays.asList(null, -1, 0, 1, 2);
        List<Integer> requestedPages = Arrays.asList(null, -1, 0, 1, 2);
        List<String> requestedSortDirections = Arrays.asList(null, "", "\t \n", "not blank");

        List<String> propertiesToSortOnWithNullString = Arrays.asList(null, "not blank");
        List<String> propertiesToSortOnWithEmptyString = Arrays.asList("", "not blank");
        List<String> propertiesToSortOnWithWhitespaceString = Arrays.asList("\t \n", "not blank");
        List<String> propertiesToSortOnWithNonBlankDuplicateStrings = Arrays.asList("not blank", "not blank");
        List<String> propertiesToSortOnWithNonBlankNonDuplicateStrings = Arrays.asList("foo", "not blank");
        List<List<String>> requestedPropertiesToSortOns = Arrays.asList(
            null,
            Arrays.asList(),
            propertiesToSortOnWithNullString,
            propertiesToSortOnWithEmptyString,
            propertiesToSortOnWithWhitespaceString,
            propertiesToSortOnWithNonBlankDuplicateStrings,
            propertiesToSortOnWithNonBlankNonDuplicateStrings
        );

        for(int requestedLimitsIndex = 0; requestedLimitsIndex < requestedLimits.size(); requestedLimitsIndex++){
            for(int requestedPagesIndex = 0; requestedPagesIndex < requestedPages.size(); requestedPagesIndex++){
                for(int requestedSortDirectionIndex = 0; requestedSortDirectionIndex < requestedSortDirections.size(); requestedSortDirectionIndex++){
                    for(int requestedPropertiesToSortOnIndex = 0; requestedPropertiesToSortOnIndex < requestedPropertiesToSortOns.size(); requestedPropertiesToSortOnIndex++){

                        Integer requestedLimit = requestedLimits.get(requestedLimitsIndex);
                        Integer requestedPage = requestedPages.get(requestedPagesIndex);
                        String requestedSortDirection = requestedSortDirections.get(requestedSortDirectionIndex);
                        List<String> requestedPropertiesToSortOn = requestedPropertiesToSortOns.get(requestedPropertiesToSortOnIndex);

                        // Expect
                        int expectedViolations = 0;
                        if(Arrays.asList(1, 2).contains(requestedLimitsIndex)) expectedViolations++; // Requested limit is not null and less than 1
                        if(Arrays.asList(1, 2).contains(requestedPagesIndex)) expectedViolations++; // Requested page is not null and less than 1
                        if(Arrays.asList(2, 3, 4, 5).contains(requestedPropertiesToSortOnIndex)) expectedViolations++; // Requested limit is not empty but has blank element OR not empty and no blank elements but elements are duplicates

                        // When
                        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(
                            testCrudService,
                            getAllMethod,
                            new Object[]{requestedLimit, requestedPage, requestedSortDirection, requestedPropertiesToSortOn}
                        );

                        // Assert
                        assertEquals(
                            "Requested limit = " + requestedLimit +
                            "\nRequested page = " + requestedPage +
                            "\nRequested sort direction = " + requestedSortDirection +
                            "\nRequested properties to sort on = " + requestedSortDirection,
                            expectedViolations,
                            violations.size()
                        );
                    }
                }
            }
        }
    }

    ////////////////////////////
    ///// GET ALL - OUTPUT /////
    ////////////////////////////

    @Test
    public void getAll_outputIsNull_constraintViolationOccurs(){

        // Given
        Set<OutputDto> output = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getAllMethod, output);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("getAll.<return value>", violation.getPropertyPath().toString());
    }

    @Test
    public void getAll_outputIsEmpty_noConstraintViolationOccurs(){

        // Given
        Set<OutputDto> output = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getAllMethod, output);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void getAll_outputIsNotEmptyButContainsNullElement_constraintViolationOccurs(){

        // Given
        Set<OutputDto> output = new HashSet<>();
        output.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getAllMethod, output);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("getAll.<return value>[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void getAll_outputIsNotEmptyAndNoElementsAreNullButOneIsInvalid_constraintViolationOccurs(){

        // Given
        OutputDto validOutputDto = TestHelper.getValidOutputDto();
        OutputDto invalidOutputDto = TestHelper.getInvalidOutputDto();

        Set<OutputDto> output = new HashSet<>();
        output.add(validOutputDto);
        output.add(invalidOutputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getAllMethod, output);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("getAll.<return value>[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void getAll_outputIsNotEmptyAndNoElementsAreNullAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        OutputDto validOutputDto1 = TestHelper.getValidOutputDto();
        OutputDto validOutputDto2 = TestHelper.getValidOutputDto();

        Set<OutputDto> output = new HashSet<>();
        output.add(validOutputDto1);
        output.add(validOutputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getAllMethod, output);

        // Assert
        assertEquals(0, violations.size());
    }

    ///////////////////////////
    ///// GET ONE - INPUT /////
    ///////////////////////////

    @Test
    public void getOne_inputIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, getOneMethod, new Object[]{blankString});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<TestCrudService> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getOne_inputIsNotBlank_noCOnstraintViolationOccurs(){

        // Given
        String notBlankString = "not blank";

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, getOneMethod, new Object[]{notBlankString});

        // Assert
        assertEquals(0, violations.size());
    }

    ////////////////////////////
    ///// GET ONE - OUTPUT /////
    ////////////////////////////

    @Test
    public void getOne_outputIsNull_oneConstraintViolationOccurs(){

        // Given
        Optional output = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getOneMethod, output);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("getOne.<return value>", violation.getPropertyPath().toString());
    }

    @Test
    public void getOne_outputIsNotNullButIsEmpty_noConstraintViolationOccurs(){

        // Given
        Optional<OutputDto> output = Optional.empty();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getOneMethod, output);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void getOne_outputIsNotNullAndNotEmptyButElementIsNotValid_oneConstraintViolationOccurs(){

        // Given
        Optional<OutputDto> output = Optional.of(TestHelper.getInvalidOutputDto());

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getOneMethod, output);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("getOne.<return value>.active", violation.getPropertyPath().toString());
    }

    @Test
    public void getOne_outputIsNotNullAndNotEmptyAndElementIsValid_noConstraintViolationOccurs(){

        // Given
        Optional<OutputDto> output = Optional.of(TestHelper.getValidOutputDto());

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, getOneMethod, output);

        // Assert
        assertEquals(0, violations.size());
    }

    //////////////////////////
    ///// UPDATE - INPUT /////
    //////////////////////////

    @Test
    public void update_inputDtosIsNull_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, updateMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.arg0", violation.getPropertyPath().toString());
    }


    @Test
    public void update_inputDtosIsEmpty_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, updateMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.arg0", violation.getPropertyPath().toString());
    }

    @Test
    public void update_inputDtosIsNotEmptyButHasNullElement_constraintViolationOccurs(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, updateMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.arg0[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void update_inputDtosIsNotEmptyAndHasNonNullElementButIsNotValid_constraintViolationOccurs(){

        // Given
        InputDto validInputDto = TestHelper.getValidInputDto();
        InputDto invalidInputDto = TestHelper.getInvalidInputDto();

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(validInputDto);
        inputDtos.add(invalidInputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, updateMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.arg0[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void update_inputDtosIsNotEmptyAndHasNonNullElementsAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        InputDto validInputDto1 = TestHelper.getValidInputDto();
        InputDto validInputDto2 = TestHelper.getValidInputDto();

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(validInputDto1);
        inputDtos.add(validInputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, updateMethod, new Object[]{inputDtos});

        // Assert
        assertEquals(0, violations.size());
    }

    ///////////////////////////
    ///// UPDATE - OUTPUT /////
    ///////////////////////////

    @Test
    public void update_outputDtosIsNull_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, updateMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.<return value>", violation.getPropertyPath().toString());
    }


    @Test
    public void update_outputDtosIsEmpty_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, updateMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.<return value>", violation.getPropertyPath().toString());
    }

    @Test
    public void update_outputDtosIsNotEmptyButHasNullElement_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, updateMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.<return value>[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void update_outputDtosIsNotEmptyAndHasNonNullElementButIsNotValid_constraintViolationOccurs(){

        // Given
        OutputDto validOutputDto = TestHelper.getValidOutputDto();

        OutputDto invalidOutputDto = TestHelper.getInvalidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto);
        outputDtos.add(invalidOutputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, updateMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("update.<return value>[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void update_outputDtosIsNotEmptyAndHasNonNullElementsAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        OutputDto validOutputDto1 = TestHelper.getValidOutputDto();
        OutputDto validOutputDto2 = TestHelper.getValidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto1);
        outputDtos.add(validOutputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, updateMethod, outputDtos);

        // Assert
        assertEquals(0, violations.size());
    }

    //////////////////////////
    ///// DELETE - INPUT /////
    //////////////////////////

    @Test
    public void delete_idsIsNull_constraintViolationOccurs(){

        // Given
        Set<String> ids = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, deleteMethod, new Object[]{ids});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.arg0", violation.getPropertyPath().toString());
    }


    @Test
    public void delete_idsIsEmpty_constraintViolationOccurs(){

        // Given
        Set<String> ids = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, deleteMethod, new Object[]{ids});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.arg0", violation.getPropertyPath().toString());
    }

    @Test
    public void delete_idsIsNotEmptyButOneElementIsBlank_constraintViolationOccurs() {

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankId) -> {
            String notBlankId = "not blank";

            Set<String> ids = new HashSet<>();
            ids.add(notBlankId);
            ids.add(blankId);

            // When
            Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, deleteMethod, new Object[]{ids});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<TestCrudService> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("delete.arg0[].<iterable element>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void delete_idsIsNotEmptyAndHasNonNullElementsAndAllAreNotBlank_noConstraintViolationOccurs(){

        // Given
        String nonBlankId1 = "not blank 1";
        String nonBlankId2 = "not blank 2";

        Set<String> ids = new HashSet<>();
        ids.add(nonBlankId1);
        ids.add(nonBlankId2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateParameters(testCrudService, deleteMethod, new Object[]{ids});

        // Assert
        assertEquals(0, violations.size());
    }

    ///////////////////////////
    ///// DELETE - OUTPUT /////
    ///////////////////////////

    @Test
    public void delete_outputDtosIsNull_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = null;

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, deleteMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.<return value>", violation.getPropertyPath().toString());
    }


    @Test
    public void delete_outputDtosIsEmpty_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, deleteMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.<return value>", violation.getPropertyPath().toString());
    }

    @Test
    public void delete_outputDtosIsNotEmptyButHasNullElement_constraintViolationOccurs(){

        // Given
        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(null);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, deleteMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.<return value>[].<iterable element>", violation.getPropertyPath().toString());
    }

    @Test
    public void delete_outputDtosIsNotEmptyAndHasNonNullElementButIsNotValid_constraintViolationOccurs(){

        // Given
        OutputDto validOutputDto = TestHelper.getValidOutputDto();

        OutputDto invalidOutputDto = TestHelper.getInvalidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto);
        outputDtos.add(invalidOutputDto);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, deleteMethod, outputDtos);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<TestCrudService> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals("delete.<return value>[].active", violation.getPropertyPath().toString());
    }

    @Test
    public void delete_outputDtosIsNotEmptyAndHasNonNullElementsAndAllAreValid_noConstraintViolationOccurs(){

        // Given
        OutputDto validOutputDto1 = TestHelper.getValidOutputDto();
        OutputDto validOutputDto2 = TestHelper.getValidOutputDto();

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(validOutputDto1);
        outputDtos.add(validOutputDto2);

        // When
        Set<ConstraintViolation<TestCrudService>> violations = executableValidator.validateReturnValue(testCrudService, deleteMethod, outputDtos);

        // Assert
        assertEquals(0, violations.size());
    }
}
