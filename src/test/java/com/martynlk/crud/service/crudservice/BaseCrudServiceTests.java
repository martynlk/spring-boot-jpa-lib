package com.martynlk.crud.service.crudservice;

import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.entity.BaseEntity;
import com.martynlk.crud.mapper.BaseMapper;
import com.martynlk.crud.message.BaseMessages;
import com.martynlk.crud.service.crudservice.valueretriever.CrudServiceValueRetriever;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BaseCrudServiceTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    public class TestBaseEntity extends BaseEntity {

        /**
         * Capabale of updating what should be immutable fields from the
         * point-of-view of {@link BaseEntity} but not the {@code active}
         * status (provides verification that {@link BaseCrudService#update(Set)}
         * handles this).
         *
         * @param entityWithUpdates
         */
        @Override
        public void update(@NotNull Object entityWithUpdates) {
            TestBaseEntity castEntityWithUpdates = (TestBaseEntity) entityWithUpdates;
            this.setId(castEntityWithUpdates.getId());
            this.setCreated(castEntityWithUpdates.getCreated());
            this.setLastModified(castEntityWithUpdates.getLastModified());
            this.setCreatedBy(castEntityWithUpdates.getCreatedBy());
        }
    }

    public class NaughtyInputDto extends InputDto {
        private LocalDateTime created;
        private LocalDateTime lastModified;
        private String createdBy;

        public LocalDateTime getCreated() {
            return created;
        }

        public void setCreated(LocalDateTime created) {
            this.created = created;
        }

        public LocalDateTime getLastModified() {
            return lastModified;
        }

        public void setLastModified(LocalDateTime lastModified) {
            this.lastModified = lastModified;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }
    }

    @Mock
    private JpaRepository<TestBaseEntity, UUID> jpaRepositoryMock;

    @Mock
    private BaseMapper<InputDto, TestBaseEntity, OutputDto> baseServiceMapperMock;

    @Mock
    private BaseMessages baseMessagesMock;

    @Mock
    private CrudServiceValueRetriever crudServiceValueRetriever;

    @InjectMocks
    private BaseCrudService baseCrudService;

    //////////////////
    ///// CREATE /////
    //////////////////

    @Test
    public void create_numberOfInputDtosPassedIsGreaterThanLimit_invalidParameterExceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        inputDto1.setId(UUID.randomUUID());

        InputDto inputDto2 = new InputDto();
        inputDto2.setId(UUID.randomUUID());

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int createLimit = inputDtos.size() - 1;
        when(crudServiceValueRetriever.getCreateLimit()).thenReturn(createLimit);

        String createLimitExceededMessage = "foo";
        when(baseMessagesMock.createLimitExceeded(anyInt(), anyInt())).thenReturn(createLimitExceededMessage);

        // Expect
        exceptionRule.expect(InvalidParameterException.class);
        exceptionRule.expectMessage(createLimitExceededMessage);

        // When
        baseCrudService.create(inputDtos);
    }

    @Test
    public void create_numberOfInputDtosPassedIsEqualToLimitButCreatedAndLastModifiedAndCreatedByFieldsAreSet_entitiesSavedWithIdAndActiveAsExpectedButOtherFieldsAreNullAndOutputIsAsExpected() {

        //Given
        NaughtyInputDto inputDto1 = new NaughtyInputDto();
        inputDto1.setId(UUID.randomUUID());
        NaughtyInputDto inputDto2 = new NaughtyInputDto();
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int createLimit = inputDtos.size();
        when(crudServiceValueRetriever.getCreateLimit()).thenReturn(createLimit);

        TestBaseEntity entityToCreate1 = new TestBaseEntity();
        entityToCreate1.setId(inputDto1.getId());
        entityToCreate1.setActive(true);
        entityToCreate1.setCreated(LocalDateTime.now());
        entityToCreate1.setLastModified(LocalDateTime.now());
        entityToCreate1.setCreatedBy("Jim");
        TestBaseEntity entityToCreate2 = new TestBaseEntity();
        entityToCreate2.setId(inputDto2.getId());
        entityToCreate2.setActive(false);
        entityToCreate2.setCreated(LocalDateTime.now());
        entityToCreate2.setLastModified(LocalDateTime.now());
        entityToCreate2.setCreatedBy("bob");
        when(baseServiceMapperMock.inputDtoToEntity(any())).thenReturn(entityToCreate1, entityToCreate2);

        TestBaseEntity entityCreated1 = new TestBaseEntity();
        TestBaseEntity entityCreated2 = new TestBaseEntity();
        entityCreated1.setId(UUID.randomUUID());
        entityCreated2.setId(UUID.randomUUID());
        when(jpaRepositoryMock.saveAll(any())).thenReturn(Arrays.asList(entityCreated1, entityCreated2));

        OutputDto outputDto1 = new OutputDto();
        OutputDto outputDto2 = new OutputDto();
        outputDto1.setId(UUID.randomUUID());
        outputDto2.setId(UUID.randomUUID());
        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(outputDto1);
        outputDtos.add(outputDto2);
        when(baseServiceMapperMock.entityToOutputDto(any())).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.create(inputDtos);

        // Verify
        ArgumentCaptor<InputDto> inputDtoArgumentCaptor = ArgumentCaptor.forClass(InputDto.class);
        ArgumentCaptor<Set<TestBaseEntity>> entitySetCaptor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<TestBaseEntity> entityCaptor = ArgumentCaptor.forClass(TestBaseEntity.class);
        verify(baseServiceMapperMock, times(2)).inputDtoToEntity(inputDtoArgumentCaptor.capture());
        verify(jpaRepositoryMock).saveAll(entitySetCaptor.capture());
        verify(baseServiceMapperMock, times(2)).entityToOutputDto(entityCaptor.capture());

        // Assert
        List<InputDto> inputDtosMapped = inputDtoArgumentCaptor.getAllValues();
        assertEquals(2, inputDtosMapped.size());
        assertTrue(inputDtosMapped.contains(inputDto1));
        assertTrue(inputDtosMapped.contains(inputDto2));

        Set<TestBaseEntity> entitySetPassedToSaveAll = entitySetCaptor.getValue();
        assertEquals(2, entitySetPassedToSaveAll.size());
        assertTrue(entitySetPassedToSaveAll.contains(entityToCreate1));
        assertTrue(entitySetPassedToSaveAll.contains(entityToCreate2));
        entitySetPassedToSaveAll.forEach((entityPassedToSaveAll) -> {

            if (entityPassedToSaveAll.getId().equals(entityToCreate1.getId())) {
                assertEquals(entityToCreate1.isActive(), entityPassedToSaveAll.isActive());
            } else if (entityPassedToSaveAll.getId().equals(entityToCreate2.getId())) {
                assertEquals(entityToCreate2.isActive(), entityPassedToSaveAll.isActive());
            } else {
                fail("No entities saved have the ID of the input DTO specified");
            }

            assertNull(entityPassedToSaveAll.getCreated());
            assertNull(entityPassedToSaveAll.getLastModified());
            assertNull(entityPassedToSaveAll.getCreatedBy());
        });


        List<TestBaseEntity> testBaseEntitiesMapped = entityCaptor.getAllValues();
        assertEquals(2, testBaseEntitiesMapped.size());
        assertTrue(testBaseEntitiesMapped.contains(entityCreated1));
        assertTrue(testBaseEntitiesMapped.contains(entityCreated2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }

    @Test
    public void create_numberOfInputDtosPassedIsLessThanLimitButCreatedAndLastModifiedAndCreatedByFieldsAreSet_entitiesSavedWithIdAndActiveAsExpectedButOtherFieldsAreNullAndOutputIsAsExpected() {

        //Given
        NaughtyInputDto inputDto1 = new NaughtyInputDto();
        inputDto1.setId(UUID.randomUUID());
        NaughtyInputDto inputDto2 = new NaughtyInputDto();
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int createLimit = inputDtos.size();
        when(crudServiceValueRetriever.getCreateLimit()).thenReturn(createLimit);

        TestBaseEntity entityToCreate1 = new TestBaseEntity();
        entityToCreate1.setId(inputDto1.getId());
        entityToCreate1.setActive(true);
        entityToCreate1.setCreated(LocalDateTime.now());
        entityToCreate1.setLastModified(LocalDateTime.now());
        entityToCreate1.setCreatedBy("Jim");
        TestBaseEntity entityToCreate2 = new TestBaseEntity();
        entityToCreate2.setId(inputDto2.getId());
        entityToCreate2.setActive(false);
        entityToCreate2.setCreated(LocalDateTime.now());
        entityToCreate2.setLastModified(LocalDateTime.now());
        entityToCreate2.setCreatedBy("bob");
        when(baseServiceMapperMock.inputDtoToEntity(any())).thenReturn(entityToCreate1, entityToCreate2);

        TestBaseEntity entityCreated1 = new TestBaseEntity();
        TestBaseEntity entityCreated2 = new TestBaseEntity();
        entityCreated1.setId(UUID.randomUUID());
        entityCreated2.setId(UUID.randomUUID());
        when(jpaRepositoryMock.saveAll(any())).thenReturn(Arrays.asList(entityCreated1, entityCreated2));

        OutputDto outputDto1 = new OutputDto();
        OutputDto outputDto2 = new OutputDto();
        outputDto1.setId(UUID.randomUUID());
        outputDto2.setId(UUID.randomUUID());
        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(outputDto1);
        outputDtos.add(outputDto2);
        when(baseServiceMapperMock.entityToOutputDto(any())).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.create(inputDtos);

        // Verify
        ArgumentCaptor<InputDto> inputDtoArgumentCaptor = ArgumentCaptor.forClass(InputDto.class);
        ArgumentCaptor<Set<TestBaseEntity>> entitySetCaptor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<TestBaseEntity> entityCaptor = ArgumentCaptor.forClass(TestBaseEntity.class);
        verify(baseServiceMapperMock, times(2)).inputDtoToEntity(inputDtoArgumentCaptor.capture());
        verify(jpaRepositoryMock).saveAll(entitySetCaptor.capture());
        verify(baseServiceMapperMock, times(2)).entityToOutputDto(entityCaptor.capture());

        // Assert
        List<InputDto> inputDtosMapped = inputDtoArgumentCaptor.getAllValues();
        assertEquals(2, inputDtosMapped.size());
        assertTrue(inputDtosMapped.contains(inputDto1));
        assertTrue(inputDtosMapped.contains(inputDto2));

        Set<TestBaseEntity> entitySetPassedToSaveAll = entitySetCaptor.getValue();
        assertEquals(2, entitySetPassedToSaveAll.size());
        assertTrue(entitySetPassedToSaveAll.contains(entityToCreate1));
        assertTrue(entitySetPassedToSaveAll.contains(entityToCreate2));
        entitySetPassedToSaveAll.forEach((entityPassedToSaveAll) -> {

            if (entityPassedToSaveAll.getId().equals(entityToCreate1.getId())) {
                assertEquals(entityToCreate1.isActive(), entityPassedToSaveAll.isActive());
            } else if (entityPassedToSaveAll.getId().equals(entityToCreate2.getId())) {
                assertEquals(entityToCreate2.isActive(), entityPassedToSaveAll.isActive());
            } else {
                fail("No entities saved have the ID of the input DTO specified");
            }

            assertNull(entityPassedToSaveAll.getCreated());
            assertNull(entityPassedToSaveAll.getLastModified());
            assertNull(entityPassedToSaveAll.getCreatedBy());
        });


        List<TestBaseEntity> testBaseEntitiesMapped = entityCaptor.getAllValues();
        assertEquals(2, testBaseEntitiesMapped.size());
        assertTrue(testBaseEntitiesMapped.contains(entityCreated1));
        assertTrue(testBaseEntitiesMapped.contains(entityCreated2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }

    ///////////////////
    ///// GET ALL /////
    ///////////////////

    @Test
    public void getAll_allInputVariationsUsedAndCrudServiceValueRetrieverReturnsPageAs1AndReadLimitAs1AndAllPossibleSortDirectionsAndPropertiesToSort_inputsPassedToCrudServiceValueRetrieverAndPageRequestBuiltAsExpected() {

        // Given
        int pageReturned = 1;
        given(crudServiceValueRetriever.getPage(any())).willReturn(pageReturned);

        int readLimitReturned = 100;
        given(crudServiceValueRetriever.getReadLimit(any(), any())).willReturn(readLimitReturned);

        List<String> propertiesToSortOnReturned = new ArrayList<>();
        propertiesToSortOnReturned.add("created");
        propertiesToSortOnReturned.add("lastModified");
        given(crudServiceValueRetriever.getPropertiesToSortOn(any())).willReturn(propertiesToSortOnReturned);

        Arrays.asList(Sort.Direction.values()).forEach((sortDirectionReturned) -> {
            given(crudServiceValueRetriever.getSortDirectionToUse(any())).willReturn(sortDirectionReturned);

            List<List<String>> requestedPropertiesToSortOnList = new ArrayList<>();
            requestedPropertiesToSortOnList.add(null);
            requestedPropertiesToSortOnList.add(new ArrayList<>());
            List<String> requestedPropertiesToSortOnStrings = new ArrayList<>();
            requestedPropertiesToSortOnStrings.add(null);
            requestedPropertiesToSortOnStrings.add("");
            requestedPropertiesToSortOnStrings.add("\t \n");
            requestedPropertiesToSortOnStrings.add("not blank");
            requestedPropertiesToSortOnList.add(requestedPropertiesToSortOnStrings);

            Arrays.asList(null, 1).forEach((requestedLimit) -> {
                Arrays.asList(null, 1).forEach((requestedPage) -> {
                    Arrays.asList(null, "", "\t \n", "not blank").forEach((requestedSortDirection) -> {
                        requestedPropertiesToSortOnList.forEach((requestedPropertiesToSortOn) -> {

                            TestBaseEntity testBaseEntity1 = new TestBaseEntity();
                            testBaseEntity1.setId(UUID.randomUUID());
                            TestBaseEntity testBaseEntity2 = new TestBaseEntity();
                            testBaseEntity2.setId(UUID.randomUUID());
                            Page<TestBaseEntity> entitiesToReturn = new PageImpl<>(Arrays.asList(testBaseEntity1, testBaseEntity2));
                            given(jpaRepositoryMock.findAll(any(PageRequest.class))).willReturn(entitiesToReturn);

                            OutputDto outputDto1 = new OutputDto();
                            outputDto1.setId(testBaseEntity1.getId());
                            OutputDto outputDto2 = new OutputDto();
                            outputDto2.setId(testBaseEntity2.getId());
                            given(baseServiceMapperMock.entityToOutputDto(any())).willReturn(outputDto1, outputDto2);

                            // Expect
                            String[] expectedPropertiesToSortOn = new String[propertiesToSortOnReturned.size()];
                            for (int i = 0; i < propertiesToSortOnReturned.size(); i++) {
                                expectedPropertiesToSortOn[i] = propertiesToSortOnReturned.get(i);
                            }

                            // When
                            Set<OutputDto> result = baseCrudService.getAll(requestedLimit, requestedPage, requestedSortDirection, requestedPropertiesToSortOn);

                            // Verify
                            verify(crudServiceValueRetriever).getPropertiesToSortOn(requestedPropertiesToSortOn);
                            verify(crudServiceValueRetriever).getPage(requestedPage);
                            verify(crudServiceValueRetriever).getReadLimit(requestedLimit, baseMessagesMock);
                            verify(crudServiceValueRetriever).getSortDirectionToUse(requestedSortDirection);
                            verify(jpaRepositoryMock).findAll(PageRequest.of(
                                pageReturned,
                                readLimitReturned,
                                sortDirectionReturned,
                                expectedPropertiesToSortOn
                            ));
                            verify(baseServiceMapperMock).entityToOutputDto(testBaseEntity1);
                            verify(baseServiceMapperMock).entityToOutputDto(testBaseEntity2);
                            verifyNoMoreInteractions(crudServiceValueRetriever, jpaRepositoryMock, baseServiceMapperMock);
                            verifyZeroInteractions(baseMessagesMock);
                            clearInvocations(crudServiceValueRetriever, jpaRepositoryMock, baseServiceMapperMock);

                            // Assert
                            assertEquals(2, result.size());
                            assertTrue(result.contains(outputDto1));
                            assertTrue(result.contains(outputDto2));
                        });
                    });
                });
            });
        });
    }

    ///////////////////
    ///// GET ONE /////
    ///////////////////

    @Test
    public void getOne_idIsNotAValidUUID_exceptionOccurs() {

        // Given
        String id = "not a UUID";

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Invalid UUID string: not a UUID");

        // When
        baseCrudService.getOne(id);
    }

    @Test
    public void getOne_idIsAValidUUIDButNoEntityFound_emptyOptionalReturned() {

        // Given
        UUID id = UUID.randomUUID();
        when(jpaRepositoryMock.findById(any(UUID.class))).thenReturn(Optional.empty());

        // When
        Optional result = baseCrudService.getOne(id.toString());

        // Verify
        verify(jpaRepositoryMock).findById(id);
        verifyNoMoreInteractions(jpaRepositoryMock);

        //Assert
        assertFalse(result.isPresent());
    }

    @Test
    public void getOne_idIsAValidUUIDAndAnEntityIsFound_optionalContainingOutputDtoVersionOfEntityFoundReturned() {

        // Given
        UUID id = UUID.randomUUID();
        TestBaseEntity testBaseEntity = new TestBaseEntity();
        testBaseEntity.setId(id);
        when(jpaRepositoryMock.findById(any(UUID.class))).thenReturn(Optional.of(testBaseEntity));

        OutputDto outputDto = new OutputDto();
        outputDto.setId(id);
        when(baseServiceMapperMock.entityToOutputDto(any())).thenReturn(outputDto);

        // When
        Optional result = baseCrudService.getOne(id.toString());

        // Verify
        verify(jpaRepositoryMock).findById(id);
        verify(baseServiceMapperMock).entityToOutputDto(testBaseEntity);
        verifyNoMoreInteractions(jpaRepositoryMock);

        //Assert
        assertEquals(outputDto, result.get());
    }

    //////////////////
    ///// UPDATE /////
    //////////////////

    @Test
    public void update_numberOfInputDtosPassedIsGreaterThanUpdateLimit_invalidParameterExceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        inputDto1.setId(UUID.randomUUID());

        InputDto inputDto2 = new InputDto();
        inputDto2.setId(UUID.randomUUID());

        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int updateLimit = inputDtos.size() - 1;
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        String updateLimitExceededMessage = "foo";
        when(baseMessagesMock.updateLimitExceeded(anyInt(), anyInt())).thenReturn(updateLimitExceededMessage);

        // Expect
        exceptionRule.expect(InvalidParameterException.class);
        exceptionRule.expectMessage(updateLimitExceededMessage);

        // When
        baseCrudService.update(inputDtos);
    }

    @Test
    public void update_numberOfInputDtosPassedIsLessThanTheUpdateLimitButTheirIdsAreNotMappedToAnyEntitiesInDatasource_exceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        InputDto inputDto2 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int updateLimit = inputDtos.size() + 1;
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList());

        String noEntityFoundMessage = "foo";
        when(baseMessagesMock.noEntityFound(any(Set.class))).thenReturn(noEntityFoundMessage);

        // Expect
        exceptionRule.expect(EntityNotFoundException.class);

        // When
        baseCrudService.update(inputDtos);
    }

    @Test
    public void update_numberOfInputDtosPassedIsLessThanTheUpdateLimitButSomeIdsAreNotMappedToEntitiesInDatasource_exceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        InputDto inputDto2 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int updateLimit = inputDtos.size() + 1;
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        TestBaseEntity testBaseEntity1 = new TestBaseEntity();
        testBaseEntity1.setId(inputDto1.getId());
        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList(testBaseEntity1));

        String noEntityFoundMessage = "foo";
        when(baseMessagesMock.noEntityFound(any(Set.class))).thenReturn(noEntityFoundMessage);

        // Expect
        exceptionRule.expect(EntityNotFoundException.class);

        // When
        baseCrudService.update(inputDtos);
    }

    @Test
    public void update_numberOfInputDtosPassedIsLessThanTheUpdateLimitAndAllIdsAreMappedToEntitiesInDatasourceAndInputsAttemptToUpdateFieldsThatShouldBeImmutable_entitiesSavedHaveCorrectValuesForImmutableFieldsAndOutputProduced() {

        //Given
        InputDto inputDto1 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        InputDto inputDto2 = new InputDto();
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);
        int updateLimit = inputDtos.size() + 1;
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        boolean entity1OriginalActive = true;
        LocalDateTime entity1OriginalCreated = LocalDateTime.of(1997, 8, 26, 12, 5, 30);
        LocalDateTime entity1OriginalLastModified = LocalDateTime.of(1998, 9, 27, 13, 6, 31);
        String entity1OriginalCreatedBy = "Bob";
        boolean entity2OriginalActive = false;
        LocalDateTime entity2OriginalCreated = LocalDateTime.of(1999, 10, 28, 14, 7, 32);
        LocalDateTime entity2OriginalLastModified = LocalDateTime.of(2000, 11, 29, 15, 8, 33);
        String entity2OriginalCreatedBy = "Jim";
        TestBaseEntity entity1 = new TestBaseEntity();
        entity1.setId(inputDto1.getId());
        entity1.setActive(entity1OriginalActive);
        entity1.setCreated(entity1OriginalCreated);
        entity1.setLastModified(entity1OriginalLastModified);
        entity1.setCreatedBy(entity1OriginalCreatedBy);
        TestBaseEntity entity2 = new TestBaseEntity();
        entity2.setId(inputDto2.getId());
        entity2.setActive(entity2OriginalActive);
        entity2.setCreated(entity2OriginalCreated);
        entity2.setLastModified(entity2OriginalLastModified);
        entity2.setCreatedBy(entity2OriginalCreatedBy);
        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList(entity1, entity2));

        TestBaseEntity entity1WithUpdates = new TestBaseEntity();
        entity1WithUpdates.setId(entity1.getId());
        entity1WithUpdates.setActive(!entity1.isActive());
        entity1WithUpdates.setCreated(entity1.getCreated().plusYears(10));
        entity1WithUpdates.setLastModified(entity1.getLastModified().plusYears(10));
        entity1WithUpdates.setCreatedBy(entity2.getCreatedBy());
        TestBaseEntity entity2WithUpdates = new TestBaseEntity();
        entity2WithUpdates.setId(entity2.getId());
        entity2WithUpdates.setActive(!entity2.isActive());
        entity2WithUpdates.setCreated(entity2.getCreated().plusYears(10));
        entity2WithUpdates.setLastModified(entity2.getLastModified().plusYears(10));
        entity2WithUpdates.setCreatedBy(entity1.getCreatedBy());

        /**
         * Note that the order of the elements returned here doesn't match the order of their corresponding elements
         * in the list returned by the JPA repo mock. This will test whether the elements are correctly sorted so the
         * updates are applied correctly.
         */
        when(baseServiceMapperMock.inputDtoToEntity(any(InputDto.class))).thenReturn(entity2WithUpdates, entity1WithUpdates);

        TestBaseEntity updatedEntity1 = new TestBaseEntity();
        updatedEntity1.setId(entity1.getId());
        TestBaseEntity updatedEntity2 = new TestBaseEntity();
        updatedEntity2.setId(entity2.getId());
        when(jpaRepositoryMock.saveAll(anyIterable())).thenReturn(Arrays.asList(updatedEntity1, updatedEntity2));

        OutputDto outputDto1 = new OutputDto();
        outputDto1.setId(updatedEntity1.getId());
        OutputDto outputDto2 = new OutputDto();
        outputDto2.setId(updatedEntity2.getId());
        when(baseServiceMapperMock.entityToOutputDto(any(TestBaseEntity.class))).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.update(inputDtos);

        // Verify
        ArgumentCaptor<Set<UUID>> uuidListCaptor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<InputDto> inputDtoCaptor = ArgumentCaptor.forClass(InputDto.class);
        ArgumentCaptor<List<TestBaseEntity>> testBaseEntityListCaptor = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<TestBaseEntity> testBaseEntityCaptor = ArgumentCaptor.forClass(TestBaseEntity.class);

        verify(jpaRepositoryMock).findAllById(uuidListCaptor.capture());
        verify(baseServiceMapperMock, times(2)).inputDtoToEntity(inputDtoCaptor.capture());
        verify(jpaRepositoryMock).saveAll(testBaseEntityListCaptor.capture());
        verify(baseServiceMapperMock, times(2)).entityToOutputDto(testBaseEntityCaptor.capture());

        // Assert
        Set<UUID> listOfUuidsUsedToSearchWith = uuidListCaptor.getValue();
        assertEquals(2, listOfUuidsUsedToSearchWith.size());
        assertTrue(listOfUuidsUsedToSearchWith.contains(inputDto1.getId()));
        assertTrue(listOfUuidsUsedToSearchWith.contains(inputDto2.getId()));

        List<InputDto> listOfInputDtosMappedToEntities = inputDtoCaptor.getAllValues();
        assertEquals(2, listOfInputDtosMappedToEntities.size());
        assertTrue(listOfInputDtosMappedToEntities.contains(inputDto1));
        assertTrue(listOfInputDtosMappedToEntities.contains(inputDto2));

        List<TestBaseEntity> listOfTestBaseEntitiesSaved = testBaseEntityListCaptor.getValue();
        assertEquals(2, listOfTestBaseEntitiesSaved.size());
        listOfTestBaseEntitiesSaved.forEach((testBaseEntitySaved) -> {
            if (testBaseEntitySaved.getId().equals(inputDto1.getId())) {
                assertEquals(entity1WithUpdates.isActive(), testBaseEntitySaved.isActive());
                assertEquals(entity1OriginalCreated, testBaseEntitySaved.getCreated());
                assertEquals(entity1OriginalLastModified, testBaseEntitySaved.getLastModified());
                assertEquals(entity1OriginalCreatedBy, testBaseEntitySaved.getCreatedBy());
            } else if (testBaseEntitySaved.getId().equals(inputDto2.getId())) {
                assertEquals(entity2WithUpdates.isActive(), testBaseEntitySaved.isActive());
                assertEquals(entity2OriginalCreated, testBaseEntitySaved.getCreated());
                assertEquals(entity2OriginalLastModified, testBaseEntitySaved.getLastModified());
                assertEquals(entity2OriginalCreatedBy, testBaseEntitySaved.getCreatedBy());
            } else {
                fail("No entities saved have the ID of the input DTO specified");
            }
        });

        List<TestBaseEntity> entitiesMappedToOutputDtos = testBaseEntityCaptor.getAllValues();
        assertEquals(2, entitiesMappedToOutputDtos.size());
        assertTrue(entitiesMappedToOutputDtos.contains(updatedEntity1));
        assertTrue(entitiesMappedToOutputDtos.contains(updatedEntity2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }

    @Test
    public void update_numberOfInputDtosPassedIsEqualToUpdateLimitButTheirIdsAreNotMappedToAnyEntitiesInDatasource_exceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        InputDto inputDto2 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int updateLimit = inputDtos.size();
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList());

        String noEntityFoundMessage = "foo";
        when(baseMessagesMock.noEntityFound(any(Set.class))).thenReturn(noEntityFoundMessage);

        // Expect
        exceptionRule.expect(EntityNotFoundException.class);

        // When
        baseCrudService.update(inputDtos);
    }

    @Test
    public void update_numberOfInputDtosPassedIsEqualToUpdateLimitButSomeIdsAreNotMappedToEntitiesInDatasource_exceptionThrown() {

        //Given
        InputDto inputDto1 = new InputDto();
        InputDto inputDto2 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);

        int updateLimit = inputDtos.size();
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        TestBaseEntity testBaseEntity1 = new TestBaseEntity();
        testBaseEntity1.setId(inputDto1.getId());
        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList(testBaseEntity1));

        String noEntityFoundMessage = "foo";
        when(baseMessagesMock.noEntityFound(any(Set.class))).thenReturn(noEntityFoundMessage);

        // Expect
        exceptionRule.expect(EntityNotFoundException.class);

        // When
        baseCrudService.update(inputDtos);
    }

    @Test
    public void update_numberOfInputDtosPassedIsEqualToUpdateLimitAndAllIdsAreMappedToEntitiesInDatasourceAndInputsAttemptToUpdateFieldsThatShouldBeImmutable_entitiesSavedHaveCorrectValuesForImmutableFieldsAndOutputProduced() {

        //Given
        InputDto inputDto1 = new InputDto();
        inputDto1.setId(UUID.randomUUID());
        InputDto inputDto2 = new InputDto();
        inputDto2.setId(UUID.randomUUID());
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(inputDto1);
        inputDtos.add(inputDto2);
        int updateLimit = inputDtos.size();
        when(crudServiceValueRetriever.getUpdateLimit()).thenReturn(updateLimit);

        boolean entity1OriginalActive = true;
        LocalDateTime entity1OriginalCreated = LocalDateTime.of(1997, 8, 26, 12, 5, 30);
        LocalDateTime entity1OriginalLastModified = LocalDateTime.of(1998, 9, 27, 13, 6, 31);
        String entity1OriginalCreatedBy = "Bob";
        boolean entity2OriginalActive = false;
        LocalDateTime entity2OriginalCreated = LocalDateTime.of(1999, 10, 28, 14, 7, 32);
        LocalDateTime entity2OriginalLastModified = LocalDateTime.of(2000, 11, 29, 15, 8, 33);
        String entity2OriginalCreatedBy = "Jim";
        TestBaseEntity entity1 = new TestBaseEntity();
        entity1.setId(inputDto1.getId());
        entity1.setActive(entity1OriginalActive);
        entity1.setCreated(entity1OriginalCreated);
        entity1.setLastModified(entity1OriginalLastModified);
        entity1.setCreatedBy(entity1OriginalCreatedBy);
        TestBaseEntity entity2 = new TestBaseEntity();
        entity2.setId(inputDto2.getId());
        entity2.setActive(entity2OriginalActive);
        entity2.setCreated(entity2OriginalCreated);
        entity2.setLastModified(entity2OriginalLastModified);
        entity2.setCreatedBy(entity2OriginalCreatedBy);
        when(jpaRepositoryMock.findAllById(any())).thenReturn(Arrays.asList(entity1, entity2));

        TestBaseEntity entity1WithUpdates = new TestBaseEntity();
        entity1WithUpdates.setId(entity1.getId());
        entity1WithUpdates.setActive(!entity1.isActive());
        entity1WithUpdates.setCreated(entity1.getCreated().plusYears(10));
        entity1WithUpdates.setLastModified(entity1.getLastModified().plusYears(10));
        entity1WithUpdates.setCreatedBy(entity2.getCreatedBy());
        TestBaseEntity entity2WithUpdates = new TestBaseEntity();
        entity2WithUpdates.setId(entity2.getId());
        entity2WithUpdates.setActive(!entity2.isActive());
        entity2WithUpdates.setCreated(entity2.getCreated().plusYears(10));
        entity2WithUpdates.setLastModified(entity2.getLastModified().plusYears(10));
        entity2WithUpdates.setCreatedBy(entity1.getCreatedBy());

        /**
         * Note that the order of the elements returned here doesn't match the order of their corresponding elements
         * in the list returned by the JPA repo mock. This will test whether the elements are correctly sorted so the
         * updates are applied correctly.
         */
        when(baseServiceMapperMock.inputDtoToEntity(any(InputDto.class))).thenReturn(entity2WithUpdates, entity1WithUpdates);

        TestBaseEntity updatedEntity1 = new TestBaseEntity();
        updatedEntity1.setId(entity1.getId());
        TestBaseEntity updatedEntity2 = new TestBaseEntity();
        updatedEntity2.setId(entity2.getId());
        when(jpaRepositoryMock.saveAll(anyIterable())).thenReturn(Arrays.asList(updatedEntity1, updatedEntity2));

        OutputDto outputDto1 = new OutputDto();
        outputDto1.setId(updatedEntity1.getId());
        OutputDto outputDto2 = new OutputDto();
        outputDto2.setId(updatedEntity2.getId());
        when(baseServiceMapperMock.entityToOutputDto(any(TestBaseEntity.class))).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.update(inputDtos);

        // Verify
        ArgumentCaptor<Set<UUID>> uuidListCaptor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<InputDto> inputDtoCaptor = ArgumentCaptor.forClass(InputDto.class);
        ArgumentCaptor<List<TestBaseEntity>> testBaseEntityListCaptor = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<TestBaseEntity> testBaseEntityCaptor = ArgumentCaptor.forClass(TestBaseEntity.class);

        verify(jpaRepositoryMock).findAllById(uuidListCaptor.capture());
        verify(baseServiceMapperMock, times(2)).inputDtoToEntity(inputDtoCaptor.capture());
        verify(jpaRepositoryMock).saveAll(testBaseEntityListCaptor.capture());
        verify(baseServiceMapperMock, times(2)).entityToOutputDto(testBaseEntityCaptor.capture());

        // Assert
        Set<UUID> listOfUuidsUsedToSearchWith = uuidListCaptor.getValue();
        assertEquals(2, listOfUuidsUsedToSearchWith.size());
        assertTrue(listOfUuidsUsedToSearchWith.contains(inputDto1.getId()));
        assertTrue(listOfUuidsUsedToSearchWith.contains(inputDto2.getId()));

        List<InputDto> listOfInputDtosMappedToEntities = inputDtoCaptor.getAllValues();
        assertEquals(2, listOfInputDtosMappedToEntities.size());
        assertTrue(listOfInputDtosMappedToEntities.contains(inputDto1));
        assertTrue(listOfInputDtosMappedToEntities.contains(inputDto2));

        List<TestBaseEntity> listOfTestBaseEntitiesSaved = testBaseEntityListCaptor.getValue();
        assertEquals(2, listOfTestBaseEntitiesSaved.size());
        listOfTestBaseEntitiesSaved.forEach((testBaseEntitySaved) -> {
            if (testBaseEntitySaved.getId().equals(inputDto1.getId())) {
                assertEquals(entity1WithUpdates.isActive(), testBaseEntitySaved.isActive());
                assertEquals(entity1OriginalCreated, testBaseEntitySaved.getCreated());
                assertEquals(entity1OriginalLastModified, testBaseEntitySaved.getLastModified());
                assertEquals(entity1OriginalCreatedBy, testBaseEntitySaved.getCreatedBy());
            } else if (testBaseEntitySaved.getId().equals(inputDto2.getId())) {
                assertEquals(entity2WithUpdates.isActive(), testBaseEntitySaved.isActive());
                assertEquals(entity2OriginalCreated, testBaseEntitySaved.getCreated());
                assertEquals(entity2OriginalLastModified, testBaseEntitySaved.getLastModified());
                assertEquals(entity2OriginalCreatedBy, testBaseEntitySaved.getCreatedBy());
            } else {
                fail("No entities saved have the ID of the input DTO specified");
            }
        });

        List<TestBaseEntity> entitiesMappedToOutputDtos = testBaseEntityCaptor.getAllValues();
        assertEquals(2, entitiesMappedToOutputDtos.size());
        assertTrue(entitiesMappedToOutputDtos.contains(updatedEntity1));
        assertTrue(entitiesMappedToOutputDtos.contains(updatedEntity2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }

    //////////////////
    ///// DELETE /////
    //////////////////

    @Test
    public void delete_numberOfInputsIsGreaterThanLimit_exceptionThrown() {

        // Given
        UUID input1 = UUID.randomUUID();
        UUID input2 = UUID.randomUUID();
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2.toString());

        int deleteLimit = inputs.size() - 1;
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        String exceptionMessage = "foo";
        when(baseMessagesMock.deleteLimitExceeded(anyInt(), anyInt())).thenReturn(exceptionMessage);

        // Expect
        exceptionRule.expect(InvalidParameterException.class);
        exceptionRule.expectMessage(exceptionMessage);

        // When
        baseCrudService.delete(inputs);
    }

    @Test
    public void delete_inputsIsEqualToLimitButContainsStringThatCanNotBeConvertedToUuid_exceptionThrown() {

        // Given
        UUID input1 = UUID.randomUUID();
        String input2 = "not a uuid";
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2);

        int deleteLimit = inputs.size();
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Invalid UUID string: not a uuid");

        // When
        baseCrudService.delete(inputs);
    }

    @Test
    public void delete_inputsIsEqualToLimitAndAllCanBeConvertedToUuidsButNotAllTheseAreMappedToInactiveDatasourceEntities_exceptionThrown() {

        // Given
        UUID input1 = UUID.randomUUID();
        UUID input2 = UUID.randomUUID();
        UUID input3 = UUID.randomUUID();
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2.toString());
        inputs.add(input3.toString());

        int deleteLimit = inputs.size();
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        /**
         * Note that there isn't a corresponding entity for input 2
         */
        TestBaseEntity entityFound1 = new TestBaseEntity();
        entityFound1.setId(input1);
        entityFound1.setActive(false);
        TestBaseEntity entityFound3 = new TestBaseEntity();
        entityFound3.setId(input3);
        entityFound3.setActive(true);

        String exceptionMessage = "foo";
        when(baseMessagesMock.entitiesToDeleteAreActive(anySetOf(String.class))).thenReturn(exceptionMessage);

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(exceptionMessage);

        // When
        baseCrudService.delete(inputs);
    }

    @Test
    public void delete_inputsIsEqualToLimitAndAllCanBeConvertedToUuidsAndAllAreMappedToInactiveDatasourceEntities_entitiesPassedToDeleteAllInBatchAndOutputReturnedAsExpected(){

        // Given
        UUID input1 = UUID.randomUUID();
        UUID input2 = UUID.randomUUID();
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2.toString());

        int deleteLimit = inputs.size();
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        TestBaseEntity entityFound1 = new TestBaseEntity();
        entityFound1.setId(input1);
        entityFound1.setActive(false);
        TestBaseEntity entityFound2 = new TestBaseEntity();
        entityFound2.setId(input2);
        entityFound2.setActive(false);
        when(jpaRepositoryMock.findAllById(anyIterableOf(UUID.class))).thenReturn(Arrays.asList(entityFound1, entityFound2));

        OutputDto outputDto1 = new OutputDto();
        outputDto1.setId(UUID.randomUUID());
        OutputDto outputDto2 = new OutputDto();
        outputDto2.setId(UUID.randomUUID());
        when(baseServiceMapperMock.entityToOutputDto(any(TestBaseEntity.class))).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.delete(inputs);

        // Verify
        ArgumentCaptor<Set<UUID>> uuidsToSearchFor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<Set<TestBaseEntity>> entitiesToDelete = ArgumentCaptor.forClass(Set.class);
        verify(jpaRepositoryMock).findAllById(uuidsToSearchFor.capture());
        verify(jpaRepositoryMock).deleteInBatch(entitiesToDelete.capture());

        // Assert
        Set<UUID> uuidsSearchedFor = uuidsToSearchFor.getValue();
        assertEquals(2, uuidsSearchedFor.size());
        assertTrue(uuidsSearchedFor.contains(input1));
        assertTrue(uuidsSearchedFor.contains(input2));

        Set<TestBaseEntity> entitiesDeleted = entitiesToDelete.getValue();
        assertEquals(2, entitiesDeleted.size());
        assertTrue(entitiesDeleted.contains(entityFound1));
        assertTrue(entitiesDeleted.contains(entityFound2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }

    @Test
    public void delete_inputsIsLessThanLimitButContainsStringThatCanNotBeConvertedToUuid_exceptionThrown() {

        // Given
        UUID input1 = UUID.randomUUID();
        String input2 = "not a uuid";
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2);

        int deleteLimit = inputs.size() + 1;
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Invalid UUID string: not a uuid");

        // When
        baseCrudService.delete(inputs);
    }

    @Test
    public void delete_inputsIsLessThanLimitAndAllCanBeConvertedToUuidsButNotAllTheseAreMappedToInactiveDatasourceEntities_exceptionThrown() {

        // Given
        UUID input1 = UUID.randomUUID();
        UUID input2 = UUID.randomUUID();
        UUID input3 = UUID.randomUUID();
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2.toString());
        inputs.add(input3.toString());

        int deleteLimit = inputs.size() + 1;
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        /**
         * Note that there isn't a corresponding entity for input 2
         */
        TestBaseEntity entityFound1 = new TestBaseEntity();
        entityFound1.setId(input1);
        entityFound1.setActive(false);
        TestBaseEntity entityFound3 = new TestBaseEntity();
        entityFound3.setId(input3);
        entityFound3.setActive(true);

        String exceptionMessage = "foo";
        when(baseMessagesMock.entitiesToDeleteAreActive(anySetOf(String.class))).thenReturn(exceptionMessage);

        // Expect
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(exceptionMessage);

        // When
        baseCrudService.delete(inputs);
    }

    @Test
    public void delete_inputsIsLessThanLimitAndAllCanBeConvertedToUuidsAndAllAreMappedToInactiveDatasourceEntities_entitiesPassedToDeleteAllInBatchAndOutputReturnedAsExpected(){

        // Given
        UUID input1 = UUID.randomUUID();
        UUID input2 = UUID.randomUUID();
        Set<String> inputs = new HashSet<>();
        inputs.add(input1.toString());
        inputs.add(input2.toString());

        int deleteLimit = inputs.size() + 1;
        when(crudServiceValueRetriever.getDeleteLimit()).thenReturn(deleteLimit);

        TestBaseEntity entityFound1 = new TestBaseEntity();
        entityFound1.setId(input1);
        entityFound1.setActive(false);
        TestBaseEntity entityFound2 = new TestBaseEntity();
        entityFound2.setId(input2);
        entityFound2.setActive(false);
        when(jpaRepositoryMock.findAllById(anyIterableOf(UUID.class))).thenReturn(Arrays.asList(entityFound1, entityFound2));

        OutputDto outputDto1 = new OutputDto();
        outputDto1.setId(UUID.randomUUID());
        OutputDto outputDto2 = new OutputDto();
        outputDto2.setId(UUID.randomUUID());
        when(baseServiceMapperMock.entityToOutputDto(any(TestBaseEntity.class))).thenReturn(outputDto1, outputDto2);

        // When
        Set<OutputDto> result = baseCrudService.delete(inputs);

        // Verify
        ArgumentCaptor<Set<UUID>> uuidsToSearchFor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<Set<TestBaseEntity>> entitiesToDelete = ArgumentCaptor.forClass(Set.class);
        verify(jpaRepositoryMock).findAllById(uuidsToSearchFor.capture());
        verify(jpaRepositoryMock).deleteInBatch(entitiesToDelete.capture());

        // Assert
        Set<UUID> uuidsSearchedFor = uuidsToSearchFor.getValue();
        assertEquals(2, uuidsSearchedFor.size());
        assertTrue(uuidsSearchedFor.contains(input1));
        assertTrue(uuidsSearchedFor.contains(input2));

        Set<TestBaseEntity> entitiesDeleted = entitiesToDelete.getValue();
        assertEquals(2, entitiesDeleted.size());
        assertTrue(entitiesDeleted.contains(entityFound1));
        assertTrue(entitiesDeleted.contains(entityFound2));

        assertEquals(2, result.size());
        assertTrue(result.contains(outputDto1));
        assertTrue(result.contains(outputDto2));
    }
}