package com.martynlk.crud.service.crudservice.valueretriever;

import com.martynlk.crud.configuration.CrudServiceConfiguration;
import com.martynlk.crud.message.BaseMessages;
import org.hibernate.validator.constraints.UniqueElements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrudServiceValueRetrieverIT.CrudServiceValueRetrieverITApplication.class)
public class CrudServiceValueRetrieverIT {

    @SpringBootApplication
    public static class CrudServiceValueRetrieverITApplication {

        @Bean
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("test");
        }
    }

    @Autowired private ValidatorFactory validatorFactory;
    private ExecutableValidator executableValidator;

    @MockBean private CrudServiceConfiguration crudServiceConfiguration;

    private CrudServiceValueRetriever crudServiceValueRetriever = new CrudServiceValueRetriever() {
        @Override public int getCreateLimit() { return 0; }
        @Override public int getReadLimit(Integer limitRequested, BaseMessages baseMessages) { return 0; }
        @Override public int getPage(Integer pageRequested) { return 0; }
        @Override public Sort.Direction getSortDirectionToUse(String sortDirectionRequested) { return null; }
        @Override public List<String> getPropertiesToSortOn(List<String> propertiesRequestedToSortOn) { return null; }
        @Override public int getUpdateLimit() { return 0; }
        @Override public int getDeleteLimit() { return 0; }
    };

    private Method getCreateLimitMethod;
    private Method getReadLimitMethod;
    private Method getPageMethod;
    private Method getSortDirectionToUseMethod;
    private Method getPropertiesToSortOnMethod;
    private Method getUpdateLimitMethod;
    private Method getDeleteLimitMethod;

    @Before
    public void setup(){
        executableValidator = validatorFactory.getValidator().forExecutables();

        try {
            getCreateLimitMethod = CrudServiceValueRetriever.class.getMethod("getCreateLimit");
            getReadLimitMethod = CrudServiceValueRetriever.class.getMethod("getReadLimit", Integer.class, BaseMessages.class);
            getPageMethod = CrudServiceValueRetriever.class.getMethod("getPage",  Integer.class);
            getSortDirectionToUseMethod = CrudServiceValueRetriever.class.getMethod("getSortDirectionToUse",  String.class);
            getPropertiesToSortOnMethod = CrudServiceValueRetriever.class.getMethod("getPropertiesToSortOn", List.class);
            getUpdateLimitMethod = CrudServiceValueRetriever.class.getMethod("getUpdateLimit");
            getDeleteLimitMethod = CrudServiceValueRetriever.class.getMethod("getDeleteLimit");
        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void getCreateLimit_negativeAndZeroReturnValuesProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(-1, 0).forEach((createLimit) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getCreateLimitMethod, createLimit);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(Min.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getCreateLimit_positiveReturnValueProvided_noConstraintViolationOccurs(){

        // Given
        int createLimit = 1;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getCreateLimitMethod, createLimit);

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getReadLimit_allLimitRequestedVariationsProvidedButBaseMessagesIsNull_oneConstraintViolationOccurs(){

        // Given
        BaseMessages baseMessages = null;
        Arrays.asList(null, -1, 0, 1).forEach((limitRequested) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getReadLimitMethod, new Object[]{limitRequested, baseMessages});

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(NotNull.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getReadLimit_allLimitRequestedVariationsProvidedAndBaseMessagesIsNotNull_noConstraintViolationsOccur(){

        // Given
        BaseMessages baseMessages = mock(BaseMessages.class);
        Arrays.asList(null, -1, 0, 1).forEach((limitRequested) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getReadLimitMethod, new Object[]{limitRequested, baseMessages});

            // Assert
            assertEquals(0, constraintViolations.size());
        });
    }

    @Test
    public void getReadLimit_negativeAndZeroReturnValuesProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(-1, 0).forEach((readLimit) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getReadLimitMethod, readLimit);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(Min.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getReadLimit_positiveReturnValueProvided_noConstraintViolationOccurs(){

        // Given
        int readLimit = 1;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getReadLimitMethod, readLimit);

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getPage_allParameterVariationsProvided_noConstraintViolationsOccur(){

        // Given
        Arrays.asList(null, -1, 0, 1).forEach((pageRequested) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getPageMethod, new Object[]{pageRequested});

            // Assert
            assertEquals(0, constraintViolations.size());
        });
    }

    @Test
    public void getPage_negativeAndZeroReturnValuesProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(-1, 0).forEach((page) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPageMethod, page);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(Min.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getPage_positiveReturnValueProvided_noConstraintViolationOccurs(){

        // Given
        int page = 1;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPageMethod, page);

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getSortDirection_allInputParameterVariationsProvided_noConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n", "not blank").forEach((sortDirectionRequested) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getSortDirectionToUseMethod, new Object[]{sortDirectionRequested});

            // Assert
            assertEquals(0, constraintViolations.size());
        });
    }

    @Test
    public void getSortDirection_nullReturnValue_oneConstraintViolationOccurs(){

        // Given
        Sort.Direction sortDirection = null;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getSortDirectionToUseMethod, sortDirection);

        // Assert
        assertEquals(1, constraintViolations.size());

        ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
        assertEquals(NotNull.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void getSortDirection_allSortDIrectionsReturned_noConstraintViolationOccurs(){

        // Given
        Arrays.asList(Sort.Direction.values()).forEach((sortDirection) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getSortDirectionToUseMethod, sortDirection);

            // Assert
            assertEquals(0, constraintViolations.size());
        });
    }

    @Test
    public void getPropertiesToSortOn_nullAndEmptyInputParameterProvided_noConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, new ArrayList<String>()).forEach((propertiesRequestedToSortOn) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getPropertiesToSortOnMethod, new Object[]{propertiesRequestedToSortOn});

            // Assert
            assertEquals(0, constraintViolations.size());
        });
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyInputParameterWithBlankElementProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            List<String> propertiesRequestedToSortOn = new ArrayList<>();
            propertiesRequestedToSortOn.add("notblank");
            propertiesRequestedToSortOn.add(blankString);

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getPropertiesToSortOnMethod, new Object[]{propertiesRequestedToSortOn});

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(NotBlank.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyInputParameterWithDuplicateNonBlankElementsProvided_noConstraintViolationOccurs(){

        // Given
        List<String> propertiesRequestedToSortOn = new ArrayList<>();
        propertiesRequestedToSortOn.add("notblank");
        propertiesRequestedToSortOn.add("notblank");
        propertiesRequestedToSortOn.add("not blank again");

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getPropertiesToSortOnMethod, new Object[]{propertiesRequestedToSortOn});

        // Assert
        assertEquals(1, constraintViolations.size());

        ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
        assertEquals(UniqueElements.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyInputParameterWithUniqueNonBlankElementsProvided_noConstraintViolationOccurs(){

        // Given
        List<String> propertiesRequestedToSortOn = new ArrayList<>();
        propertiesRequestedToSortOn.add("notblank");
        propertiesRequestedToSortOn.add("not blank again");

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateParameters(crudServiceValueRetriever, getPropertiesToSortOnMethod, new Object[]{propertiesRequestedToSortOn});

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getPropertiesToSortOn_nullAndEmptyReturnValueProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, new ArrayList<String>()).forEach((propertiesToSortOn) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPropertiesToSortOnMethod, propertiesToSortOn);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(NotEmpty.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyReturnValueWithBlankElementProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            List<String> propertiesToSortOn = new ArrayList<>();
            propertiesToSortOn.add("notblank");
            propertiesToSortOn.add(blankString);

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPropertiesToSortOnMethod, propertiesToSortOn);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(NotBlank.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyReturnValueWithNonBlankDuplicateElementsProvided_noConstraintViolationOccurs(){

        // Given
        List<String> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add("notblank");
        propertiesToSortOn.add("notblank");
        propertiesToSortOn.add("not blank again");

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPropertiesToSortOnMethod, propertiesToSortOn);

        // Assert
        assertEquals(1, constraintViolations.size());

        ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
        assertEquals(UniqueElements.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void getPropertiesToSortOn_nonEmptyReturnValueWithUniqueNonBlankElementsProvided_noConstraintViolationOccurs(){

        // Given
        List<String> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add("notblank");
        propertiesToSortOn.add("not blank again");

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getPropertiesToSortOnMethod, propertiesToSortOn);

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getUpdateLimit_negativeAndZeroReturnValuesProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(-1, 0).forEach((updateLimit) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getUpdateLimitMethod, updateLimit);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(Min.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getUpdateLimit_positiveReturnValueProvided_noConstraintViolationOccurs(){

        // Given
        int updateLimit = 1;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getUpdateLimitMethod, updateLimit);

        // Assert
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void getDeleteLimit_negativeAndZeroReturnValuesProvided_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(-1, 0).forEach((deleteLimit) -> {

            // When
            Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getDeleteLimitMethod, deleteLimit);

            // Assert
            assertEquals(1, constraintViolations.size());

            ConstraintViolation<CrudServiceValueRetriever> constraintViolation = constraintViolations.iterator().next();
            assertEquals(Min.class, constraintViolation.getConstraintDescriptor().getAnnotation().annotationType());
        });
    }

    @Test
    public void getDeleteLimit_positiveReturnValueProvided_noConstraintViolationOccurs(){

        // Given
        int deleteLimit = 1;

        // When
        Set<ConstraintViolation<CrudServiceValueRetriever>> constraintViolations = executableValidator.validateReturnValue(crudServiceValueRetriever, getDeleteLimitMethod, deleteLimit);

        // Assert
        assertEquals(0, constraintViolations.size());
    }
}
