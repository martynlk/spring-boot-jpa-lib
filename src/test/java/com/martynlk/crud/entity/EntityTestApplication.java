package com.martynlk.crud.entity;

import com.martynlk.crud.configuration.JpaAuditingConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Optional;

@SpringBootApplication
@EnableJpaRepositories
public class EntityTestApplication {

    public static String AUDITOR_NAME = "jim";

    @Bean(JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME)
    public AuditorAware<String> auditorAware() {
        return () -> Optional.of(AUDITOR_NAME);
    }
}




