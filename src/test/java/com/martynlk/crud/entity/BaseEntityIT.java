package com.martynlk.crud.entity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EntityTestApplication.class)
@AutoConfigureTestEntityManager
@Transactional
public class BaseEntityIT {

    @Rule public ExpectedException exceptionRule = ExpectedException.none();

    @Autowired private TestEntityManager testEntityManager;
    @Autowired private TestJpaRepository testJpaRepository;

    private TestEntity testEntity;

    @Before
    public void setup(){
        testEntity = new TestEntity();
        testEntity.setActive(true);
        testEntity = testEntityManager.persistAndFlush(testEntity);
    }

    @Test
    public void updateId_savedTestEntityHasIdUpdatedThenSaved_exceptionOccurs(){

        // Given
        UUID initialId = testEntity.getId();
        UUID generatedId = UUID.randomUUID();
        testEntity.setId(generatedId);

        // Expect
        exceptionRule.expect(JpaSystemException.class);
        exceptionRule.expectMessage("identifier of an instance of " + TestEntity.class.getCanonicalName() +
            " was altered from " + initialId.toString() + " to " + generatedId.toString());

        // When
        testJpaRepository.saveAndFlush(testEntity);
    }

    @Test
    public void updateId_savedTestEntityHasActiveUpdatedToNullThenSaved_exceptionOccurs(){

        // Given
        testEntity.setActive(null);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage(
    "Validation failed for classes [" + TestEntity.class.getCanonicalName() + "] during update time for " +
            "groups [javax.validation.groups.Default, ]"
        );

        // When
        testJpaRepository.saveAndFlush(testEntity);
    }

    @Test
    public void updateId_savedTestEntityHasActiveUpdatedToFalseThenSaved_activeStatusUpdated(){

        // Given
        testEntity.setActive(false);

        // When
        TestEntity updatedTestEntity = testJpaRepository.saveAndFlush(testEntity);

        // Assert
        assertFalse(updatedTestEntity.isActive());
    }

    @Test
    public void updateId_validEntity_entitySavedAsExpected(){

        // Given
        TestEntity testEntity = new TestEntity();
        testEntity.setActive(true);

        // When
        TestEntity savedTestEntity = testJpaRepository.saveAndFlush(testEntity);

        // Assert
        assertTrue(Objects.nonNull(savedTestEntity.getId()));
        assertTrue(savedTestEntity.isActive());
        assertEquals(EntityTestApplication.AUDITOR_NAME, savedTestEntity.getCreatedBy());
        assertTrue(Objects.nonNull(savedTestEntity.getCreated()));
        assertTrue(Objects.nonNull(savedTestEntity.getLastModified()));
    }

    @Test
    public void updateId_validEntitySavedThenActiveUpdatedAndAuditorNameChangedThenSavedAgain_entityUpdatedAsExpected(){

        // Given
        TestEntity testEntity = new TestEntity();
        testEntity.setActive(true);
        TestEntity savedTestEntity = testJpaRepository.saveAndFlush(testEntity);

        UUID initialId = savedTestEntity.getId();
        Boolean initialActive = savedTestEntity.isActive();
        String initialCreatedBy = savedTestEntity.getCreatedBy();
        LocalDateTime initialCreated = savedTestEntity.getCreated();
        LocalDateTime initialLastModified = savedTestEntity.getLastModified();

        // When
        savedTestEntity.setActive(false);
        String newAuditorName = "Bob";
        EntityTestApplication.AUDITOR_NAME = newAuditorName;
        TestEntity updatedTestEntity = testJpaRepository.saveAndFlush(savedTestEntity);

        // Assert
        assertEquals(initialId, updatedTestEntity.getId());
        assertNotEquals(initialActive, updatedTestEntity.isActive());
        assertFalse(updatedTestEntity.isActive());
        assertNotEquals(newAuditorName, updatedTestEntity.getCreatedBy());
        assertEquals(initialCreatedBy, updatedTestEntity.getCreatedBy());
        assertEquals(initialCreated, updatedTestEntity.getCreated());
        assertTrue(updatedTestEntity.getLastModified().isAfter(initialLastModified));
    }
}
