package com.martynlk.crud.dto;

import com.martynlk.crud.configuration.JpaAuditingConfiguration;
import com.martynlk.springbootbeanvalidation.annotation.LocalDateTimeIsNotBeforeOther;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OutputDtoIT {

    @SpringBootApplication
    public static class TestApplication {

        @Bean(JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME)
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("Foo");
        }
    }

    @Autowired
    private ValidatorFactory validatorFactory;
    private Validator validator;

    // No fields are null, String fields aren't blank, and `created` is equal to `lastModified`.
    private OutputDto validOutputDto;

    private static final String ID_FIELD_NAME = "id";
    private static final String ACTIVE_FIELD_NAME = "active";
    private static final String CREATED_BY_FIELD_NAME = "createdBy";
    private static final String CREATED_FIELD_NAME = "created";
    private static final String LAST_MODIFIED_FIELD_NAME = "lastModified";

    @Before
    public void setup(){
        validator = validatorFactory.getValidator();

        validOutputDto = new OutputDto();
        validOutputDto.setId(UUID.randomUUID());
        validOutputDto.setActive(true);
        validOutputDto.setCreatedBy("Foo");
        validOutputDto.setCreated(LocalDateTime.of(2019, 2, 10, 17, 56));
        validOutputDto.setLastModified(validOutputDto.getCreated());
    }

    @Test
    public void validate_dtoIsSetupAsValid_noConstraintViolationsOccur(){

        // Given
        OutputDto outputDto = validOutputDto;

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_dtoIsOtherwiseValidButIdIsNull_oneConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setId(null);

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<OutputDto> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(ID_FIELD_NAME, violation.getPropertyPath().toString());
    }

    @Test
    public void validate_dtoIsOtherwiseValidButActiveIsNull_oneConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setActive(null);

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<OutputDto> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(ACTIVE_FIELD_NAME, violation.getPropertyPath().toString());
    }

    @Test
    public void validate_dtoIsOtherwiseValidANdActiveIsFalse_noConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setActive(false);

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_dtoIsOtherwiseValidButCreatedByIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {
            OutputDto outputDto = validOutputDto;
            outputDto.setCreatedBy(blankString);

            // When
            Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<OutputDto> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals(CREATED_BY_FIELD_NAME, violation.getPropertyPath().toString());
        });
    }

    @Test
    public void validate_dtoIsOtherwiseValidButCreatedIsNull_oneConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setCreated(null);

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<OutputDto> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(CREATED_FIELD_NAME, violation.getPropertyPath().toString());
    }

    @Test
    public void validate_dtoIsOtherwiseValidButLastModifiedIsNull_oneConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setLastModified(null);

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<OutputDto> violation = violations.iterator().next();
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(LAST_MODIFIED_FIELD_NAME, violation.getPropertyPath().toString());
    }

    @Test
    public void validate_dtoIsOtherwiseValidAndCreatedIsBeforeLastModified_noConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setCreated(LocalDateTime.now());
        outputDto.setLastModified(outputDto.getCreated().plusSeconds(1l));

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_dtoIsOtherwiseValidButCreatedIsAfterLastModified_oneConstraintViolationOccurs(){

        // Given
        OutputDto outputDto = validOutputDto;
        outputDto.setCreated(LocalDateTime.now());
        outputDto.setLastModified(outputDto.getCreated().minusSeconds(1l));

        // When
        Set<ConstraintViolation<OutputDto>> violations = validator.validate(outputDto);

        // Assert
        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<OutputDto> violation = violations.iterator().next();
        assertEquals(LocalDateTimeIsNotBeforeOther.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }
}
