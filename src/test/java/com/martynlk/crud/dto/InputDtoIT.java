package com.martynlk.crud.dto;

import com.martynlk.crud.annotation.group.Create;
import com.martynlk.crud.annotation.group.Update;
import com.martynlk.crud.configuration.JpaAuditingConfiguration;
import com.martynlk.springbootbeanvalidation.annotation.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InputDtoIT {

    @SpringBootApplication
    public static class TestApplication {

        @Bean(JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME)
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("Foo");
        }
    }

    @Autowired private ValidatorFactory validatorFactory;
    private Validator validator;

    private static final String ACTIVE_FIELD_NAME = "active";
    private static final String ID_FIELD_NAME = "id";

    @Before
    public void setup(){
        validator = validatorFactory.getValidator();
    }

    //////////////////////
    ///// NO CONTEXT /////
    //////////////////////

    @Test
    public void validate_validatedInNoContextAndIdIsNullAndActiveIsNull_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(null);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ACTIVE_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInNoContextAndIdIsNullAndActiveIsTrue_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInNoContextAndIdIsNullAndActiveIsFalse_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInNoContextAndIdIsNotNullAndActiveIsNull_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(null);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ACTIVE_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInNoContextAndIdIsNotNullAndActiveIsTrue_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInNoContextAndIdIsNotNullAndActiveIsFalse_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto);

        // Assert
        assertEquals(0, violations.size());
    }

    //////////////////////////
    ///// CREATE CONTEXT /////
    //////////////////////////

    @Test
    public void validate_validatedInCreateContextAndIdIsNullAndActiveIsNull_oneConstraintViolationsOccur(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(null);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Create.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ACTIVE_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInCreateContextAndIdIsNullAndActiveIsTrue_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Create.class});

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInCreateContextAndIdIsNullAndActiveIsFalse_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Create.class});

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInCreateContextAndIdIsNotNullAndActiveIsNull_twoConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(null);

        // When
        List<ConstraintViolation<InputDto>> violations = validator
            .validate(inputDto, new Class[]{Create.class})
            .stream()
            .sorted((constraintViolation1, constraintViolation2) -> {
                String constraintViolation1PropertyPath = constraintViolation1.getPropertyPath().toString();
                String constraintViolation2PropertyPath = constraintViolation2.getPropertyPath().toString();
                return constraintViolation1PropertyPath.compareTo(constraintViolation2PropertyPath);
            })
            .collect(Collectors.toList())
        ;

        // Assert
        assertEquals(2, violations.size());

        assertEquals(ACTIVE_FIELD_NAME, violations.get(0).getPropertyPath().toString());
        assertEquals(NotNull.class, violations.get(0).getConstraintDescriptor().getAnnotation().annotationType());

        assertEquals(ID_FIELD_NAME, violations.get(1).getPropertyPath().toString());
        assertEquals(IsNull.class, violations.get(1).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInCreateContextAndIdIsNotNullAndActiveIsTrue_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Create.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(IsNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(ID_FIELD_NAME, violation.getPropertyPath().toString());
    }

    @Test
    public void validate_validatedInCreateContextAndIdIsNotNullAndActiveIsFalse_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Create.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(IsNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
        assertEquals(ID_FIELD_NAME, violation.getPropertyPath().toString());
    }

    //////////////////////////
    ///// UPDATE CONTEXT /////
    //////////////////////////

    @Test
    public void validate_validatedInUpdateContextAndIdIsNullAndActiveIsNull_twoConstraintViolationsOccur(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(null);

        // When
        List<ConstraintViolation<InputDto>> violations = validator
            .validate(inputDto, new Class[]{Update.class})
            .stream()
            .sorted((constraintViolation1, constraintViolation2) -> {
                String constraintViolation1PropertyPath = constraintViolation1.getPropertyPath().toString();
                String constraintViolation2PropertyPath = constraintViolation2.getPropertyPath().toString();
                return constraintViolation1PropertyPath.compareTo(constraintViolation2PropertyPath);
            })
            .collect(Collectors.toList());
        ;

        // Assert
        assertEquals(2, violations.size());

        assertEquals(ACTIVE_FIELD_NAME, violations.get(0).getPropertyPath().toString());
        assertEquals(NotNull.class, violations.get(0).getConstraintDescriptor().getAnnotation().annotationType());

        assertEquals(ID_FIELD_NAME, violations.get(1).getPropertyPath().toString());
        assertEquals(NotNull.class, violations.get(1).getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInUpdateContextAndIdIsNullAndActiveIsTrue_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Update.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ID_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInUpdateContextAndIdIsNullAndActiveIsFalse_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(null);
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Update.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ID_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInUpdateContextAndIdIsNotNullAndActiveIsNull_oneConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(null);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Update.class});

        // Assert
        assertEquals(1, violations.size());

        ConstraintViolation<InputDto> violation = violations.iterator().next();
        assertEquals(ACTIVE_FIELD_NAME, violation.getPropertyPath().toString());
        assertEquals(NotNull.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
    }

    @Test
    public void validate_validatedInUpdateContextAndIdIsNotNullAndActiveIsTrue_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(true);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Update.class});

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void validate_validatedInUpdateContextAndIdIsNotNullAndActiveIsFalse_noConstraintViolationOccurs(){

        // Given
        InputDto inputDto = new InputDto();
        inputDto.setId(UUID.randomUUID());
        inputDto.setActive(false);

        // When
        Set<ConstraintViolation<InputDto>> violations = validator.validate(inputDto, new Class[]{Update.class});

        // Assert
        assertEquals(0, violations.size());
    }
}
