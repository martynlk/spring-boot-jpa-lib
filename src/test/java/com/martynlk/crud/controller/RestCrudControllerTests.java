package com.martynlk.crud.controller;

import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.service.crudservice.CrudService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RestCrudControllerTests {

    @Mock private CrudService crudServiceMock;
    @InjectMocks private RestCrudController restCrudController;

    @Test
    public void create_inputAndOutputHaveNonNullElements_inputPassedToCrudServiceAndOutputReturned(){

        // Given
        Set<InputDto> inputs = new HashSet<>();
        inputs.add(new InputDto());
        Set<OutputDto> outputs = new HashSet<>();
        outputs.add(new OutputDto());

        when(crudServiceMock.create(any())).thenReturn(outputs);

        // When
        Set<OutputDto> result = restCrudController.create(inputs);

        // Verify
        verify(crudServiceMock).create(inputs);

        // Assert
        assertEquals(outputs, result);
    }

    @Test
    public void getAll_allInputAndOutputVariationsTested_inputsPassedToCrudServiceAndOutputReturned() {

        // Given
        List<Integer> limits = Arrays.asList(null, -1, 0, 1);

        List<Integer> pages = Arrays.asList(null, -2, 0, 2);

        List<String> sortDirections = new ArrayList<>();
        sortDirections.add(null);
        sortDirections.add("");
        sortDirections.add("\t \n");
        Arrays.asList(Sort.Direction.values()).forEach((direction) -> sortDirections.add(direction.name()));

        List<List<String>> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add(null);
        propertiesToSortOn.add(new ArrayList<>());
        List<String> blankAndNonBlankProperties = new ArrayList<>();
        blankAndNonBlankProperties.add(null);
        blankAndNonBlankProperties.add("");
        blankAndNonBlankProperties.add("\t \n");
        blankAndNonBlankProperties.add("created");
        propertiesToSortOn.add(blankAndNonBlankProperties);

        Set<OutputDto> nonEmptyOutputs = new HashSet<>();
        nonEmptyOutputs.add(new OutputDto());
        List<Set<OutputDto>> outputs = Arrays.asList(new LinkedHashSet<>(), nonEmptyOutputs);

        // When
        limits.forEach((limit) -> {
            pages.forEach((page) -> {
                sortDirections.forEach((sortDirection) -> {
                    propertiesToSortOn.forEach((propertiesToSort) -> {
                        outputs.forEach((output) -> {

                            when(crudServiceMock.getAll(any(), any(), any(), any())).thenReturn(output);

                            Set<OutputDto> result = restCrudController.getAll(limit, page, sortDirection, propertiesToSort);

                            // Verify
                            verify(crudServiceMock).getAll(limit, page, sortDirection, propertiesToSort);
                            clearInvocations(crudServiceMock);

                            // Assert
                            assertEquals(output, result);
                        });
                    });
                });
            });
        });
    }

    @Test
    public void getOne_inputIsNotBlankAndOutputIsNull_inputPassedToCrudServiceAndNullReturned(){

        // Given
        String input = UUID.randomUUID().toString();
        OutputDto output = null;

        when(crudServiceMock.getOne(anyString())).thenReturn(Optional.ofNullable(output));

        // When
        OutputDto result = restCrudController.getOne(input);

        // Verify
        verify(crudServiceMock).getOne(input);

        // Assert
        assertNull(result);
    }

    @Test
    public void getOne_inputIsNotBlankAndOutputIsNotEmpty_inputPassedToCrudServiceAndOutputReturned(){

        // Given
        String input = UUID.randomUUID().toString();
        OutputDto output = new OutputDto();

        when(crudServiceMock.getOne(anyString())).thenReturn(Optional.ofNullable(output));

        // When
        OutputDto result = restCrudController.getOne(input);

        // Verify
        verify(crudServiceMock).getOne(input);

        // Assert
        assertEquals(output, result);
    }

    @Test
    public void update_inputAndOutputHaveNonNullElements_inputPassedToCrudServiceAndOutputReturned(){

        // Given
        Set<InputDto> inputs = new HashSet<>();
        inputs.add(new InputDto());
        Set<OutputDto> outputs = new HashSet<>();
        outputs.add(new OutputDto());

        when(crudServiceMock.update(any())).thenReturn(outputs);

        // When
        Set<OutputDto> result = restCrudController.update(inputs);

        // Verify
        verify(crudServiceMock).update(inputs);

        // Assert
        assertEquals(outputs, result);
    }

    @Test
    public void delete_inputIsNotBlankAndOutputHasNonNullElement_inputPassedToCrudServiceAndOutputReturned(){

        // Given
        Set<String> inputs = new HashSet<>();
        inputs.add("foo");
        Set<OutputDto> outputs = new HashSet<>();
        outputs.add(new OutputDto());

        when(crudServiceMock.delete(any())).thenReturn(outputs);

        // When
        Set<OutputDto> result = restCrudController.delete(inputs);

        // Verify
        verify(crudServiceMock).delete(inputs);

        // Assert
        assertEquals(outputs, result);
    }
}
