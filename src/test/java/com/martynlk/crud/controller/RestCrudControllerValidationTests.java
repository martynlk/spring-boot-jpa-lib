package com.martynlk.crud.controller;

import com.martynlk.crud.configuration.JpaAuditingConfiguration;
import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.service.crudservice.CrudService;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestCrudControllerValidationTests.TestApplication.class)
public class RestCrudControllerValidationTests {

    @Rule public ExpectedException exceptionRule = ExpectedException.none();

    @MockBean private CrudService<InputDto, OutputDto> crudServiceMock;

    @SpringBootApplication
    public static class TestApplication {

        @Bean(JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME)
        public AuditorAware<String> auditorAware(){
            return () -> Optional.of("Foo");
        }

        @RestController
        public class TestController extends RestCrudController {

            // This is a key that can be found in `test/resources/ValidationMessages.properties` and
            // `test/resources/ValidationMessages_fr.properties`.
            private static final String GET_ONE_NOT_NULL_MESSAGE_KEY = "{com.martynlk.crud.controller.TestController.getOne.returnValue.NotNull}";

            public TestController(CrudService<InputDto, OutputDto> crudService){
                super(crudService);
            }

            // This method is provided to illustrate how a consumer can override the `@NotNull` annotation message on
            // the return value of the base `getOne()` method.
            @Override
            public @NotNull(message = GET_ONE_NOT_NULL_MESSAGE_KEY) OutputDto getOne(String id) {
                return super.getOne(id);
            }
        }
    }

    @Autowired private RestCrudController<InputDto, OutputDto> testController;

    @After
    public void teardown(){
        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void create_inputIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = null;

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.inputDtos: must not be empty");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.inputDtos: must not be empty");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(null);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.inputDtos[].<iterable element>: must not be null");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsNotEmptyAndHasNonNullElementsButReturnValueIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = null;
        when(crudServiceMock.create(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.<return value>: must not be empty");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsNotEmptyAndHasNonNullElementsButReturnValueIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        when(crudServiceMock.create(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.<return value>: must not be empty");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsNotEmptyAndHasNonNullElementsAndReturnValueIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(null);
        when(crudServiceMock.create(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("create.<return value>[].<iterable element>: must not be null");

        // When
        testController.create(inputDtos);
    }

    @Test
    public void create_inputIsNotEmptyAndHasNonNullElementsAndReturnValueIsNotEmptyAndHasNonNullElements_inputPassedToServiceAndOutputReturned(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(new OutputDto());
        when(crudServiceMock.create(any())).thenReturn(outputDtos);

        // When
        Set<OutputDto> result = testController.create(inputDtos);

        // Verify
        verify(crudServiceMock).create(inputDtos);
        verifyNoMoreInteractions(crudServiceMock);

        // Assert
        assertEquals(outputDtos, result);
    }

    @Test
    public void getAll_allInputVariationsTestedButServiceReturnsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        List<Integer> limits = Arrays.asList(null, -1, 0, 1);

        List<Integer> pages = Arrays.asList(null, -2, 0, 2);

        List<String> sortDirections = new ArrayList<>();
        sortDirections.add(null);
        sortDirections.add("");
        sortDirections.add("\t \n");
        Arrays.asList(Sort.Direction.values()).forEach((direction) -> sortDirections.add(direction.name()));

        List<List<String>> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add(null);
        propertiesToSortOn.add(new ArrayList<>());
        List<String> blankAndNonBlankProperties = new ArrayList<>();
        blankAndNonBlankProperties.add(null);
        blankAndNonBlankProperties.add("");
        blankAndNonBlankProperties.add("\t \n");
        blankAndNonBlankProperties.add("created");
        propertiesToSortOn.add(blankAndNonBlankProperties);

        Set<OutputDto> output = null;

        when(crudServiceMock.getAll(any(), any(), any(), any())).thenReturn(output);

        limits.forEach((limit) -> {
            pages.forEach((page) -> {
                sortDirections.forEach((sortDirection) -> {
                    propertiesToSortOn.forEach((propertiesToSort) -> {

                        // Expect
                        exceptionRule.expect(ConstraintViolationException.class);
                        exceptionRule.expectMessage("getAll.<return value>: must not be null");

                        // When
                        testController.getAll(limit, page, sortDirection, propertiesToSort);
                    });
                });
            });
        });
    }

    @Test
    public void getAll_allInputVariationsTestedAndServiceReturnsEmptySet_limitAndPagePassedToServiceAndOutputReturned(){

        // Given
        List<Integer> limits = Arrays.asList(null, -1, 0, 1);

        List<Integer> pages = Arrays.asList(null, -2, 0, 2);

        List<String> sortDirections = new ArrayList<>();
        sortDirections.add(null);
        sortDirections.add("");
        sortDirections.add("\t \n");
        Arrays.asList(Sort.Direction.values()).forEach((direction) -> sortDirections.add(direction.name()));

        List<List<String>> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add(null);
        propertiesToSortOn.add(new ArrayList<>());
        List<String> blankAndNonBlankProperties = new ArrayList<>();
        blankAndNonBlankProperties.add(null);
        blankAndNonBlankProperties.add("");
        blankAndNonBlankProperties.add("\t \n");
        blankAndNonBlankProperties.add("created");
        propertiesToSortOn.add(blankAndNonBlankProperties);

        Set<OutputDto> output = new HashSet<>();

        when(crudServiceMock.getAll(any(), any(), any(), any())).thenReturn(output);

        limits.forEach((limit) -> {
            pages.forEach((page) -> {
                sortDirections.forEach((sortDirection) -> {
                    propertiesToSortOn.forEach((propertiesToSort) -> {

                        // When
                        Set<OutputDto> result = testController.getAll(limit, page, sortDirection, propertiesToSort);

                        // Verify
                        verify(crudServiceMock).getAll(limit, page, sortDirection, propertiesToSort);
                        verifyNoMoreInteractions(crudServiceMock);

                        clearInvocations(crudServiceMock);

                        // Assert
                        assertTrue(result.isEmpty());
                    });
                });
            });
        });
    }

    @Test
    public void getAll_allInputVariationsTestedButServiceReturnsNonEmptyListWithNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        List<Integer> limits = Arrays.asList(null, -1, 0, 1);

        List<Integer> pages = Arrays.asList(null, -2, 0, 2);

        List<String> sortDirections = new ArrayList<>();
        sortDirections.add(null);
        sortDirections.add("");
        sortDirections.add("\t \n");
        Arrays.asList(Sort.Direction.values()).forEach((direction) -> sortDirections.add(direction.name()));

        List<List<String>> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add(null);
        propertiesToSortOn.add(new ArrayList<>());
        List<String> blankAndNonBlankProperties = new ArrayList<>();
        blankAndNonBlankProperties.add(null);
        blankAndNonBlankProperties.add("");
        blankAndNonBlankProperties.add("\t \n");
        blankAndNonBlankProperties.add("created");
        propertiesToSortOn.add(blankAndNonBlankProperties);

        Set<OutputDto> output = new HashSet<>();
        output.add(new OutputDto());
        output.add(null);

        when(crudServiceMock.getAll(any(), any(), any(), any())).thenReturn(output);

        limits.forEach((limit) -> {
            pages.forEach((page) -> {
                sortDirections.forEach((sortDirection) -> {
                    propertiesToSortOn.forEach((propertiesToSort) -> {

                        // Expect
                        exceptionRule.expect(ConstraintViolationException.class);
                        exceptionRule.expectMessage("getAll.<return value>[].<iterable element>: must not be null");

                        // When
                        testController.getAll(limit, page, sortDirection, propertiesToSort);
                    });
                });
            });
        });
    }

    @Test
    public void getAll_allInputVariationsTestedAndServiceReturnsNonEmptySetWithNonNullElements_limitAndPagePassedToServiceAndOutputReturned(){

        // Given
        List<Integer> limits = Arrays.asList(null, -1, 0, 1);

        List<Integer> pages = Arrays.asList(null, -2, 0, 2);

        List<String> sortDirections = new ArrayList<>();
        sortDirections.add(null);
        sortDirections.add("");
        sortDirections.add("\t \n");
        Arrays.asList(Sort.Direction.values()).forEach((direction) -> sortDirections.add(direction.name()));

        List<List<String>> propertiesToSortOn = new ArrayList<>();
        propertiesToSortOn.add(null);
        propertiesToSortOn.add(new ArrayList<>());
        List<String> blankAndNonBlankProperties = new ArrayList<>();
        blankAndNonBlankProperties.add(null);
        blankAndNonBlankProperties.add("");
        blankAndNonBlankProperties.add("\t \n");
        blankAndNonBlankProperties.add("created");
        propertiesToSortOn.add(blankAndNonBlankProperties);

        Set<OutputDto> output = new HashSet<>();
        output.add(new OutputDto());
        output.add(new OutputDto());

        when(crudServiceMock.getAll(any(), any(), any(), any())).thenReturn(output);

        limits.forEach((limit) -> {
            pages.forEach((page) -> {
                sortDirections.forEach((sortDirection) -> {
                    propertiesToSortOn.forEach((propertiesToSort) -> {

                        // When
                        Set<OutputDto> result = testController.getAll(limit, page, sortDirection, propertiesToSort);

                        // Verify
                        verify(crudServiceMock).getAll(limit, page, sortDirection, propertiesToSort);
                        verifyNoMoreInteractions(crudServiceMock);

                        clearInvocations(crudServiceMock);

                        // Assert
                        assertEquals(output, result);
                    });
                });
            });
        });
    }

    @Test
    public void getOne_idIsBlank_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            exceptionRule.expect(ConstraintViolationException.class);
            exceptionRule.expectMessage("getOne.id: must not be blank");

            // When
            testController.getOne(blankString);
        });
    }

    @Test
    public void getOne_idIsNotBlankButNullReturnedAndLocaleIsDefault_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        String id = "not blank";
        OutputDto output = null;
        when(crudServiceMock.getOne(any())).thenReturn(Optional.ofNullable(output));

        LocaleContextHolder.resetLocaleContext();

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("getOne.<return value>: Default message");

        // When
        testController.getOne(id);
    }

    @Test
    public void getOne_idIsNotBlankButNullReturnedAndLocaleIsFrench_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        String id = "not blank";
        OutputDto output = null;
        when(crudServiceMock.getOne(any())).thenReturn(Optional.ofNullable(output));

        LocaleContextHolder.setLocale(Locale.FRENCH);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("getOne.<return value>: French message");

        // When
        testController.getOne(id);
    }

    @Test
    public void getOne_idIsNotBlankAndNotNullObjectReturned_outputReturnedAsExpected(){

        // Given
        String id = "not blank";
        OutputDto output = new OutputDto();
        when(crudServiceMock.getOne(any())).thenReturn(Optional.ofNullable(output));

        // When
        OutputDto result = testController.getOne(id);

        // Verify
        verify(crudServiceMock).getOne(id);
        verifyNoMoreInteractions(crudServiceMock);

        // Assert
        assertEquals(output, result);
    }

    @Test
    public void update_inputIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = null;

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.inputDtos: must not be empty");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.inputDtos: must not be empty");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(null);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.inputDtos[].<iterable element>: must not be null");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsNotEmptyAndHasNonNullElementsButReturnValueIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = null;
        when(crudServiceMock.update(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.<return value>: must not be empty");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsNotEmptyAndHasNonNullElementsButReturnValueIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        when(crudServiceMock.update(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.<return value>: must not be empty");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsNotEmptyAndHasNonNullElementsAndReturnValueIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(null);
        when(crudServiceMock.update(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("update.<return value>[].<iterable element>: must not be null");

        // When
        testController.update(inputDtos);
    }

    @Test
    public void update_inputIsNotEmptyAndHasNonNullElementsAndReturnValueIsNotEmptyAndHasNonNullElements_inputPassedToServiceAndOutputReturned(){

        // Given
        Set<InputDto> inputDtos = new HashSet<>();
        inputDtos.add(new InputDto());
        inputDtos.add(new InputDto());

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(new OutputDto());
        when(crudServiceMock.update(any())).thenReturn(outputDtos);

        // When
        Set<OutputDto> result = testController.update(inputDtos);

        // Verify
        verify(crudServiceMock).update(inputDtos);
        verifyNoMoreInteractions(crudServiceMock);

        // Assert
        assertEquals(outputDtos, result);
    }

    @Test
    public void delete_inputIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = null;

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.ids: must not be empty");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.ids: must not be empty");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank");
        ids.add(null);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.ids[].<iterable element>: must not be blank");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyButHasEmptyStringElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank");
        ids.add("");

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.ids[].<iterable element>: must not be blank");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyButHasWHitespaceStringElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank");
        ids.add("\t \n");

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.ids[].<iterable element>: must not be blank");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyAndHasNoBlankElementsButReturnValueIsNull_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank 1");
        ids.add("not blank 2");

        Set<OutputDto> outputDtos = null;
        when(crudServiceMock.delete(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.<return value>: must not be empty");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyAndHasNoBlankElementsButReturnValueIsEmpty_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank 1");
        ids.add("not blank 2");

        Set<OutputDto> outputDtos = new HashSet<>();
        when(crudServiceMock.delete(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.<return value>: must not be empty");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyAndHasNoBlankElementsAndReturnValueIsNotEmptyButHasNullElement_constraintViolationExceptionWithParticularMessageThrown(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank 1");
        ids.add("not blank 2");

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(null);
        when(crudServiceMock.delete(any())).thenReturn(outputDtos);

        // Expect
        exceptionRule.expect(ConstraintViolationException.class);
        exceptionRule.expectMessage("delete.<return value>[].<iterable element>: must not be null");

        // When
        testController.delete(ids);
    }

    @Test
    public void delete_inputIsNotEmptyAndHasNoBlankElementsAndReturnValueIsNotEmptyAndHasNonNullElements_inputPassedToServiceAndOutputReturned(){

        // Given
        Set<String> ids = new HashSet<>();
        ids.add("not blank 1");
        ids.add("not blank 2");

        Set<OutputDto> outputDtos = new HashSet<>();
        outputDtos.add(new OutputDto());
        outputDtos.add(new OutputDto());
        when(crudServiceMock.delete(any())).thenReturn(outputDtos);

        // When
        Set<OutputDto> result = testController.delete(ids);

        // Verify
        verify(crudServiceMock).delete(ids);
        verifyNoMoreInteractions(crudServiceMock);

        // Assert
        assertEquals(outputDtos, result);
    }
}
