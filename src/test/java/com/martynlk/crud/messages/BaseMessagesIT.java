package com.martynlk.crud.messages;

import com.martynlk.crud.message.BaseMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseMessagesIT {

    @SpringBootApplication
    public static class BaseMessagesITApplication {
        @Bean public AuditorAware<String> auditorAware() { return () -> Optional.of("He"); }
    }

    @Autowired private ValidatorFactory validatorFactory;
    private ExecutableValidator executableValidator;

    @MockBean private MessageSource messageSourceMock;
    private String noEntityFoundMessageKey = "foo";
    private String createLimitExceededMessageKey = "bar";
    private String readLimitExceededMessageKey = "baz";
    private String updateLimitExceededMessageKey = "qux";
    private String deleteLimitExceededMessageKey = "quux";
    private String entitiesToDeleteAreActiveMessageKey = "corge";

    private BaseMessages baseMessages;

    private Method noEntityFoundMethod;
    private Method createLimitExceededMethod;
    private Method readLimitExceededMethod;
    private Method updateLimitExceededMethod;
    private Method deleteLimitExceededMethod;
    private Method entitiesToDeleteAreActiveMethod;

    @Captor private ArgumentCaptor<String> stringCaptor;
    @Captor private ArgumentCaptor<String[]> stringArrayCaptor;
    @Captor private ArgumentCaptor<Locale> localeCaptor;

    @Before
    public void setup(){
        executableValidator = validatorFactory.getValidator().forExecutables();

        baseMessages = new BaseMessages(
            messageSourceMock,
            noEntityFoundMessageKey,
            createLimitExceededMessageKey,
            readLimitExceededMessageKey,
            updateLimitExceededMessageKey,
            deleteLimitExceededMessageKey,
            entitiesToDeleteAreActiveMessageKey
        );

        try {
            noEntityFoundMethod = BaseMessages.class.getMethod("noEntityFound", Set.class);
            createLimitExceededMethod = BaseMessages.class.getMethod("createLimitExceeded", int.class, int.class);
            readLimitExceededMethod = BaseMessages.class.getMethod("readLimitExceeded", int.class, int.class);
            updateLimitExceededMethod = BaseMessages.class.getMethod("updateLimitExceeded", int.class, int.class);
            deleteLimitExceededMethod = BaseMessages.class.getMethod("deleteLimitExceeded", int.class, int.class);
            entitiesToDeleteAreActiveMethod = BaseMessages.class.getMethod("entitiesToDeleteAreActive", Set.class);
        } catch (NoSuchMethodException e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void noEntityFound_inputIsEmpty_oneConstraintViolationOccur(){

        // Given
        Arrays.asList(null, new HashSet<>()).forEach((emptySet) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, noEntityFoundMethod, new Object[]{emptySet});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("noEntityFound.ids", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void noEntityFound_inputIsNotEmptyButIdsAreBlank_oneConstraintViolationOccur(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            Set<String> ids = new HashSet<>();
            ids.add("not blank");
            ids.add(blankString);

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, noEntityFoundMethod, new Object[]{ids});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("noEntityFound.ids[].<iterable element>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void noEntityFound_inputIsNotEmptyAndIdsAreNotBlank_noConstraintViolationsOccur(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            Set<String> ids = new HashSet<>();
            ids.add("not blank");
            ids.add("not balnk again");

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, noEntityFoundMethod, new Object[]{ids});

            // Assert
            assertEquals(0, violations.size());
        });
    }

    @Test
    public void noEntityFound_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, noEntityFoundMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("noEntityFound.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void noEntityFound_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, noEntityFoundMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void noEntityFound_knownInputProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String id1 = "not blank";
        String id2 = "not blank again";
        Set<String> ids = new HashSet<>();
        ids.add(id1);
        ids.add(id2);

        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        // Expect
        String[] stringsInterpolated = new String[]{id1 + ", " + id2};

        // When
        String result = baseMessages.noEntityFound(ids);

        // Verify
        verify(messageSourceMock).getMessage(noEntityFoundMessageKey, stringsInterpolated, Locale.getDefault());

        // Assert
        assertEquals(returnValue, result);
    }

    @Test
    public void createLimitExceeded_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, createLimitExceededMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("createLimitExceeded.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void createLimitExceeded_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, createLimitExceededMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void createLimitExceeded_allCombinationsOfNegativeAndZeroAndPositiveInputsProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        int[] inputsToUse = new int[] {-1, 0, 1};
        for(int createLimit : inputsToUse) {
            for (int violatingValue : inputsToUse) {

                // Expect
                String[] stringsInterpolated = new String[]{ String.valueOf(createLimit), String.valueOf(violatingValue) };

                // When
                String result = baseMessages.createLimitExceeded(createLimit, violatingValue);

                // Verify
                verify(messageSourceMock).getMessage(createLimitExceededMessageKey, stringsInterpolated, Locale.getDefault());

                // Assert
                assertEquals(returnValue, result);
            }
        }
    }

    @Test
    public void readLimitExceeded_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, readLimitExceededMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("readLimitExceeded.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void readLimitExceeded_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, readLimitExceededMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void readLimitExceeded_allCombinationsOfNegativeAndZeroAndPositiveInputsProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        int[] inputsToUse = new int[] {-1, 0, 1};
        for(int readLimit : inputsToUse) {
            for (int violatingValue : inputsToUse) {

                // Expect
                String[] stringsInterpolated = new String[]{ String.valueOf(readLimit), String.valueOf(violatingValue) };

                // When
                String result = baseMessages.readLimitExceeded(readLimit, violatingValue);

                // Verify
                verify(messageSourceMock).getMessage(readLimitExceededMessageKey, stringsInterpolated, Locale.getDefault());

                // Assert
                assertEquals(returnValue, result);
            }
        }
    }

    @Test
    public void updateLimitExceeded_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, updateLimitExceededMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("updateLimitExceeded.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void updateLimitExceeded_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, updateLimitExceededMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void updateLimitExceeded_allCombinationsOfNegativeAndZeroAndPositiveInputsProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        int[] inputsToUse = new int[] {-1, 0, 1};
        for(int updateLimit : inputsToUse) {
            for (int violatingValue : inputsToUse) {

                // Expect
                String[] stringsInterpolated = new String[]{ String.valueOf(updateLimit), String.valueOf(violatingValue) };

                // When
                String result = baseMessages.updateLimitExceeded(updateLimit, violatingValue);

                // Verify
                verify(messageSourceMock).getMessage(updateLimitExceededMessageKey, stringsInterpolated, Locale.getDefault());

                // Assert
                assertEquals(returnValue, result);
            }
        }
    }

    @Test
    public void deleteLimitExceeded_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, deleteLimitExceededMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("deleteLimitExceeded.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void deleteLimitExceeded_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, deleteLimitExceededMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void deleteLimitExceeded_allCombinationsOfNegativeAndZeroAndPositiveInputsProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        int[] inputsToUse = new int[] {-1, 0, 1};
        for(int deleteLimit : inputsToUse) {
            for (int violatingValue : inputsToUse) {

                // Expect
                String[] stringsInterpolated = new String[]{ String.valueOf(deleteLimit), String.valueOf(violatingValue) };

                // When
                String result = baseMessages.deleteLimitExceeded(deleteLimit, violatingValue);

                // Verify
                verify(messageSourceMock).getMessage(deleteLimitExceededMessageKey, stringsInterpolated, Locale.getDefault());

                // Assert
                assertEquals(returnValue, result);
            }
        }
    }

    @Test
    public void entitiesToDeleteAreActive_inputIsEmpty_oneConstraintViolationOccur(){

        // Given
        Arrays.asList(null, new HashSet<>()).forEach((emptySet) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, entitiesToDeleteAreActiveMethod, new Object[]{emptySet});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotEmpty.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("entitiesToDeleteAreActive.ids", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void entitiesToDeleteAreActive_inputIsNotEmptyButIdsAreBlank_oneConstraintViolationOccur(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            Set<String> ids = new HashSet<>();
            ids.add("not blank");
            ids.add(blankString);

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, entitiesToDeleteAreActiveMethod, new Object[]{ids});

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("entitiesToDeleteAreActive.ids[].<iterable element>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void entitiesToDeleteAreActive_inputIsNotEmptyAndIdsAreNotBlank_noConstraintViolationsOccur(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            Set<String> ids = new HashSet<>();
            ids.add("not blank");
            ids.add("not balnk again");

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateParameters(baseMessages, entitiesToDeleteAreActiveMethod, new Object[]{ids});

            // Assert
            assertEquals(0, violations.size());
        });
    }

    @Test
    public void entitiesToDeleteAreActive_returnValueIsBlank_oneConstraintViolationOccurs(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // When
            Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, entitiesToDeleteAreActiveMethod, blankString);

            // Assert
            assertEquals(1, violations.size());

            ConstraintViolation<BaseMessages> violation = violations.iterator().next();
            assertEquals(NotBlank.class, violation.getConstraintDescriptor().getAnnotation().annotationType());
            assertEquals("entitiesToDeleteAreActive.<return value>", violation.getPropertyPath().toString());
        });
    }

    @Test
    public void entitiesToDeleteAreActive_returnValueIsNotBlank_noConstraintViolationOccurs(){

        // Given
        String returnValue = "not blank";

        // When
        Set<ConstraintViolation<BaseMessages>> violations = executableValidator.validateReturnValue(baseMessages, entitiesToDeleteAreActiveMethod, returnValue);

        // Assert
        assertEquals(0, violations.size());
    }

    @Test
    public void entitiesToDeleteAreActive_knownInputProvided_methodExecutesAndReturnValueAsExpected(){

        // Given
        String id1 = "not blank";
        String id2 = "not blank again";
        Set<String> ids = new HashSet<>();
        ids.add(id1);
        ids.add(id2);

        String returnValue = "yet again not blank";
        given(messageSourceMock.getMessage(anyString(), any(Object[].class), any(Locale.class))).willReturn(returnValue);

        // Expect
        String[] stringsInterpolated = new String[]{id1 + ", " + id2};

        // When
        String result = baseMessages.entitiesToDeleteAreActive(ids);

        // Verify
        verify(messageSourceMock).getMessage(entitiesToDeleteAreActiveMessageKey, stringsInterpolated, Locale.getDefault());

        // Assert
        assertEquals(returnValue, result);
    }
}
