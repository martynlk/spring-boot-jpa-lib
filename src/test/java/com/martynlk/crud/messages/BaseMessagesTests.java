package com.martynlk.crud.messages;

import com.martynlk.crud.message.BaseMessages;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class BaseMessagesTests {

    @Rule public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void constructor_baseMessagesIsNull_exceptionThrown(){

        // Expect
        exceptionRule.expect(NullPointerException.class);

        // When
        new BaseMessages(
            null,
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"
        );
    }

    @Test
    public void constructor_noEntityFoundMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    null,
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6"
                );
            } catch(NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_createLimitExceededMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    blankString,
                    "3",
                    "4",
                    "5",
                    "6"
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_readLimitExceededMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    "2",
                    blankString,
                    "4",
                    "5",
                    "6"
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_updateLimitExceededMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    "2",
                    "3",
                    blankString,
                    "5",
                    "6"
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_deleteLimitExceededMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    "2",
                    "3",
                    "4",
                    blankString,
                    "6"
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_entitiesToDeleteAreActiveMessageKeyIsBlank_exceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    blankString
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertTrue(exceptionThrown);
        });
    }

    @Test
    public void constructor_messageSourceIsNotBlankAndAllMessageKeysAreNotBlank_noExceptionThrown(){

        // Given
        Arrays.asList(null, "", "\t \n").forEach((blankString) -> {

            // Expect
            boolean exceptionThrown = false;

            // When
            try {
                new BaseMessages(
                    new ReloadableResourceBundleMessageSource(),
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6"
                );
            } catch (NullPointerException e){
                exceptionThrown = true;
            }

            // Assert
            assertFalse(exceptionThrown);
        });
    }
}
