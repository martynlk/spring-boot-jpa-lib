package com.martynlk.crud;

import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;

import java.time.LocalDateTime;
import java.util.UUID;

public class TestHelper {

    public static InputDto getValidInputDto(){
        InputDto validInputDto = new InputDto();
        validInputDto.setId(UUID.randomUUID());
        validInputDto.setActive(true);
        return validInputDto;
    }

    /**
     *
     * @return An {@link InputDto} whose {@code active} field is invalid
     * because it is {@code null}. Thus, it will fail {@link javax.validation.constraints.NotNull}.
     */
    public static InputDto getInvalidInputDto(){
        InputDto invalidInputDto = getValidInputDto();
        invalidInputDto.setActive(null);
        return invalidInputDto;
    }

    public static OutputDto getValidOutputDto(){
        OutputDto validOutputDto = new OutputDto();
        validOutputDto.setId(UUID.randomUUID());
        validOutputDto.setActive(true);
        validOutputDto.setCreated(LocalDateTime.now());
        validOutputDto.setLastModified(LocalDateTime.now());
        validOutputDto.setCreatedBy("Jim");
        return validOutputDto;
    }

    /**
     *
     * @return An {@link OutputDto} whose {@code active} field is invalid
     * because it is {@code null}. Thus, it will fail {@link javax.validation.constraints.NotNull}.
     */
    public static OutputDto getInvalidOutputDto(){
        OutputDto invalidOutputDto = getValidOutputDto();
        invalidOutputDto.setActive(null);
        return invalidOutputDto;
    }
}
