package com.martynlk.crud.mapper;

import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.entity.BaseEntity;

public interface BaseMapper<T extends InputDto, U extends BaseEntity, V extends OutputDto> {

    U inputDtoToEntity(T inputDto);
    V entityToOutputDto(U entity);
}
