package com.martynlk.crud.message;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Validated
public class BaseMessages {

    private final MessageSource messageSource;
    private final String noEntityFoundMessageKey;
    private final String createLimitExceededMessageKey;
    private final String readLimitExceededMessageKey;
    private final String updateLimitExceededMessageKey;
    private final String deleteLimitExceededMessageKey;
    private final String entitiesToDeleteAreActiveMessageKey;

    public BaseMessages(
        MessageSource messageSource,
        String noEntityFoundMessageKey,
        String createLimitExceededMessageKey,
        String readLimitExceededMessageKey,
        String updateLimitExceededMessageKey,
        String deleteLimitExceededMessageKey,
        String entitiesToDeleteAreActiveMessageKey
    ) {
        Objects.requireNonNull(messageSource);
        Objects.requireNonNull(StringUtils.stripToNull(noEntityFoundMessageKey));
        Objects.requireNonNull(StringUtils.stripToNull(createLimitExceededMessageKey));
        Objects.requireNonNull(StringUtils.stripToNull(readLimitExceededMessageKey));
        Objects.requireNonNull(StringUtils.stripToNull(updateLimitExceededMessageKey));
        Objects.requireNonNull(StringUtils.stripToNull(deleteLimitExceededMessageKey));
        Objects.requireNonNull(StringUtils.stripToNull(entitiesToDeleteAreActiveMessageKey));

        this.messageSource = messageSource;
        this.noEntityFoundMessageKey = noEntityFoundMessageKey;
        this.createLimitExceededMessageKey = createLimitExceededMessageKey;
        this.readLimitExceededMessageKey = readLimitExceededMessageKey;
        this.updateLimitExceededMessageKey = updateLimitExceededMessageKey;
        this.deleteLimitExceededMessageKey = deleteLimitExceededMessageKey;
        this.entitiesToDeleteAreActiveMessageKey = entitiesToDeleteAreActiveMessageKey;
    }

    public @NotBlank String noEntityFound(@NotEmpty Set<@NotBlank String> ids){
        return messageSource
            .getMessage(
                noEntityFoundMessageKey,
                new String[] { ids.stream().sorted(String::compareTo).collect(Collectors.joining(", ")) },
                LocaleContextHolder.getLocale()
            )
        ;
    }

    public @NotBlank String createLimitExceeded(int createLimit, int violatingValue){
        return messageSource
            .getMessage(
                createLimitExceededMessageKey,
                new String[]{ String.valueOf(createLimit), String.valueOf(violatingValue) },
                LocaleContextHolder.getLocale()
            );
    }

    public @NotBlank String readLimitExceeded(int readLimit, int violatingValue){
        return messageSource
            .getMessage(
                readLimitExceededMessageKey,
                new String[]{ String.valueOf(readLimit), String.valueOf(violatingValue) },
                LocaleContextHolder.getLocale()
            );
    }

    public @NotBlank String updateLimitExceeded(int updateLimit, int violatingValue){
        return messageSource
            .getMessage(
                updateLimitExceededMessageKey,
                new String[]{ String.valueOf(updateLimit), String.valueOf(violatingValue) },
                LocaleContextHolder.getLocale()
            );
    }

    public @NotBlank  String deleteLimitExceeded(int deleteLimit, int violatingValue){
        return messageSource
            .getMessage(
                deleteLimitExceededMessageKey,
                new String[]{ String.valueOf(deleteLimit), String.valueOf(violatingValue) },
                LocaleContextHolder.getLocale()
            );
    }

    public @NotBlank String entitiesToDeleteAreActive(@NotEmpty Set<@NotBlank String> ids){
        return messageSource
            .getMessage(
                entitiesToDeleteAreActiveMessageKey,
                new String[] { ids.stream().sorted(String::compareTo).collect(Collectors.joining(", ")) },
                LocaleContextHolder.getLocale()
            )
        ;
    }
}
