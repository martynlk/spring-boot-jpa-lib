package com.martynlk.crud.controller;

import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.service.crudservice.CrudService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Provides methods that link to {@link CrudService} methods for creating,
 * updating, retrieving, and deleting data from the underlying datasource.
 * <br/>
 * <br/>
 * This class can be extended by any of your controllers that can make use
 * of the methods provided. To do this, you should:
 * <ol type="1">
 *     <li>
 *         Ensure you annotate your controller class with {@link RestController}
 *         or {@link Controller}.
 *     </li>
 *     <li>
 *         Ensure you annotate your controller class with {@link RequestMapping}.
 *     </li>
 *     <li>
 *         Override any methods in this class that you want to expose as an endpoint
 *         in your controller, and annotate them with an appropriate HTTP method
 *         mapping annotation, e.g. {@link PostMapping}. Unfortunately, its not
 *         possible to set-up these annotations in this class since your consumer class
 *         will not inherit the annotations. The JavaDoc for each method in this class
 *         does advises you on what HTTP methods should be mapped to each method though
 *         (if you're uncertain!).
 *     </li>
 * </ol>
 *
 * The following example shows a simple controller that extends this class
 * and exposes {@link RestCrudController#create(Set)} as a POST HTTP method on an
 * "/example" endpoint:
 *
 * <pre>
 * &#64;RestController
 * &#64;RequestMapping(ExampleController.RESOURCE)
 * public class ExampleController
 *     &#60;T extends ExampleInputDto, U extends ExampleOutputDto&#62;
 *     extends CrudController&#60;T, U&#62;
 * {
 *
 *     public static final String RESOURCE = "/example";
 *
 *     &#64;PostMapping
 *     &#64;Override
 *     public Set&#60;U&#62; create(Set&#60;T&#62; inputDtos){
 *         return super.create(inputDtos);
 *     }
 * }
 * </pre>
 *
 * @param <T>
 * @param <U>
 */
@Validated
public class RestCrudController<T extends InputDto, U extends OutputDto> {

    protected static final String ID_REQUEST_PARAM = "id";

    private final CrudService<T, U> crudService;

    public RestCrudController(
        CrudService<T, U> crudService
    ){
        this.crudService = crudService;
    }

    /**
     * The method that overrides this method in your controller should be annotated with
     * {@link PostMapping}.
     * <br/>
     * <br/>
     * Also ensure that you annotate the {@code inputDtos} parameter in the override
     * method with {@link RequestBody}.
     * <br/>
     * <br/>
     * An example override of this method is as follows:
     * <br/>
     * <br/>
     * <pre>
     * &#64;PostMapping
     * &#64;Override
     * public Set&#60;YourOutputDtoClass&#62; create(Set&#60;YourInputDtoClass&#62; inputDtos){
     *     return super.create(inputDtos);
     * }
     * </pre>
     *
     * @param inputDtos
     *
     * @return The result of {@link CrudService#create(Set)} after
     * passing the {@code inputDtos} specified.
     *
     */
    public @NotEmpty Set<@NotNull U> create(@NotEmpty Set<@NotNull T> inputDtos){
        return crudService.create(inputDtos);
    }

    /**
     * The method that overrides this method in your controller should be annotated with
     * {@link GetMapping}.
     * <br/>
     * <br/>
     * An example override of this method is as follows:
     * <br/>
     * <br/>
     * <pre>
     * &#64;GetMapping
     * &#64;Override
     * public Set&#60;YourOutputDtoClass&#62; getAll(Integer limit, Integer page, String sortDirection, Set&#60;String&#62; propertiesToSortOn){
     *     return super.getAll(limit, page, sortDirection, propertiesToSortOn);
     * }
     * </pre>
     *
     * @param limit {@link CrudService#getAll(Integer, Integer, String, List)}.
     * @param page {@link CrudService#getAll(Integer, Integer, String, List)}.
     *
     * @return The result of {@link CrudService#getAll(Integer, Integer, String, List)}.
     *
     */
    public @NotNull Set<@NotNull U> getAll(Integer limit, Integer page, String sortDirection, List<String> propertiesToSortOn){
        return crudService.getAll(limit, page, sortDirection, propertiesToSortOn);
    }

    /**
     * The method that overrides this method in your controller should be annotated with
     * {@link GetMapping}.
     * <br/>
     * <br/>
     * <b>NOTE:</b> this class exposes a protected {@link RestCrudController#ID_REQUEST_PARAM}
     * constant which should be interpolated between <code>"/{"</code> and <code>"}"</code>
     * when used as a value for the {@link GetMapping} annotation.
     * <br/>
     * <br/>
     * Also ensure that you annotate the {@code id} parameter in the override method with
     * {@link PathVariable}.
     * <br/>
     * <br/>
     * You can also override the {@link NotNull} validation on the return object to give a
     * controller-specific error message. <b>ADVICE:</b> if you want internationalised
     * validation messages, add a <i>ValidationMessages.properties</i> file to your Sprint
     * Boot application's <i>resources</i> folder (usually located in <i>src/main/resources</i>)
     * along with any other locale-specific variations. For example, to enable French validation
     * messages: add a <i>ValidationMessages_fr.properties</i> file in the same location
     * you created your <i>ValidationMessages.properties</i> file (and ensure you call
     * {@link LocaleContextHolder#setLocale(Locale)} with {@link Locale#FRENCH} at some point!).
     * See <a href="https://docs.jboss.org/hibernate/validator/6.1/reference/en-US/html_single/#section-message-interpolation">
     * here</a> for more details.
     * <br/>
     * <br/>
     * An example override of this method is as follows:
     * <br/>
     * <br/>
     * <pre>
     * private static final String GET_ONE_MSG = "{your.controller.package.NotNull.message}";
     *
     * &#64;GetMapping("/&#123" + ID_REQUEST_PARAM + "&#125")
     * &#64;Override
     * public @NotNull(message = GET_ONE_MSG) Set&#60;YourOutputDtoClass&#62; getOne(&#64;PathVariable String id){
     *     return super.getOne(id);
     * }
     * </pre>
     *
     * @param id
     *
     * @return The result of {@link CrudService#getOne(String)} after
     * passing the {@code id} specified.
     */
    public @NotNull U getOne(@NotBlank String id) {
        return crudService.getOne(id).orElse(null);
    }

    /**
     * The method that overrides this method in your controller should be annotated with {@link PutMapping}.
     * <br/>
     * <br/>
     * Also ensure that you annotate the {@code inputDtos} parameter in the override method with
     * {@link RequestBody}.
     * <br/>
     * <br/>
     * An example override of this method is as follows:
     * <br/>
     * <br/>
     * <pre>
     * &#64;PutMapping
     * &#64;Override
     * public Set&#60;YourOutputDtoClass&#62; update(Set&#60;YourInputDtoClass&#62; inputDtos){
     *     return super.update(inputDtos);
     * }
     * </pre>
     *
     * @param inputDtos
     *
     * @return The result of {@link CrudService#update(Set)} after passing
     * the {@code inputDtos} specified.
     *
     */
    public @NotEmpty Set<@NotNull U> update(@NotEmpty Set<@NotNull T> inputDtos){
        return crudService.update(inputDtos);
    }

    /**
     * The method that overrides this method in your controller should be annotated with {@link DeleteMapping}.
     * <br/>
     * <br/>
     * Also ensure that you annotate the {@code ids} parameter in the override method with {@link RequestBody}.
     * <br/>
     * <br/>
     * An example override of this method is as follows:
     * <br/>
     * <br/>
     * <pre>
     * &#64;DeleteMapping
     * &#64;Override
     * public Set&#60;YourOutputDtoClass&#62; delete(Set&#60;String&#62; ids){
     *     return super.delete(ids);
     * }
     * </pre>
     *
     * @param ids
     *
     * @return The result of {@link CrudService#delete(Set)} after passing
     * the {@code ids} specified.
     *
     */
    public @NotEmpty Set<@NotNull U> delete(@NotEmpty Set<@NotBlank String> ids){
        return crudService.delete(ids);
    }
}
