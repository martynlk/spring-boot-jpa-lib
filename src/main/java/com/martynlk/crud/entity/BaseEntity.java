package com.martynlk.crud.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@DynamicUpdate
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @NotNull
    private Boolean active;

    @CreatedBy
    private String createdBy;

    @CreatedDate
    private LocalDateTime created;
 
    @LastModifiedDate
    private LocalDateTime lastModified;
    
    /**
     * Required by Hibernate
     */
    public BaseEntity() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Should define how fields are updated in this BaseEntity given the
     * {@code entityWithUpdates} passed. <b>NOTE:</b> the {@code entityWithUpdates}
     * will need to be cast to your class that extends BaseEntity before anything
     * useful can be done with it!
     *
     * @param entityWithUpdates
     */
    public abstract void update(@NotNull Object entityWithUpdates);

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof BaseEntity)){
            return false;
        }
        BaseEntity rhs = (BaseEntity) obj;
        return new EqualsBuilder()
            .append(this.id, rhs.id)
            .append(this.active, rhs.active)
            .append(this.createdBy, rhs.createdBy)
            .append(this.created, rhs.created)
            .append(this.lastModified, rhs.lastModified)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(id)
            .append(active)
            .append(createdBy)
            .append(created)
            .append(lastModified)
            .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("id", id)
            .append("active", active)
            .append("createdBy", createdBy)
            .append("created", created)
            .append("lastModified", lastModified)
            .toString();
    }
}
