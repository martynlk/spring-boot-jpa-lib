package com.martynlk.crud.dto;

import com.martynlk.springbootbeanvalidation.annotation.LocalDateTimeIsNotBeforeOther;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@LocalDateTimeIsNotBeforeOther(
    propertyNameToCheck = OutputDto.LAST_MODIFIED_FIELD_NAME,
    propertyNameToCheckAgainst = OutputDto.CREATED_FIELD_NAME
)
public class OutputDto {

    protected static final String CREATED_FIELD_NAME = "created";
    protected static final String LAST_MODIFIED_FIELD_NAME = "lastModified";

    @NotNull
    private UUID id;

    @NotNull
    private Boolean active;

    @NotBlank
    private String createdBy;

    @NotNull
    private LocalDateTime created;

    @NotNull
    private LocalDateTime lastModified;
    
    public OutputDto() {
    }

    public OutputDto(
        UUID id,
        Boolean active,
        String createdBy, 
        LocalDateTime created, 
        LocalDateTime lastModified
    ) {
        this.id = id;
        this.active = active;
        this.createdBy = createdBy;
        this.created = created;
        this.lastModified = lastModified;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof OutputDto)) {
            return false;
        }
        OutputDto castOther = (OutputDto) other;
        return new EqualsBuilder()
            .append(id, castOther.id)
            .append(active, castOther.active)
            .append(createdBy, castOther.createdBy)
            .append(created, castOther.created)
            .append(lastModified, castOther.lastModified)
            .isEquals()
        ;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(id)
            .append(active)
            .append(createdBy)
            .append(created)
            .append(lastModified)
            .toHashCode()
        ;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("id", id)
            .append("active", active)
            .append("createdBy", createdBy)
            .append("created", created)
            .append("lastModified", lastModified)
            .toString()
        ;
    }
}
