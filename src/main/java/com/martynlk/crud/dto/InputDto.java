package com.martynlk.crud.dto;

import com.martynlk.crud.annotation.group.Create;
import com.martynlk.crud.annotation.group.Update;
import com.martynlk.springbootbeanvalidation.annotation.IsNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.util.UUID;

public class InputDto {


    @IsNull(groups = Create.class)
    @NotNull(groups = Update.class)
    private UUID id;

    @NotNull(groups = {Default.class, Create.class, Update.class})
    private Boolean active;
    
    public InputDto() {
    }

    public InputDto(
        UUID id,
        Boolean active
    ) {
        this.id = id;
        this.active = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof InputDto)) {
            return false;
        }
        InputDto rhs = (InputDto) obj;
        return new EqualsBuilder()
            .append(this.id, rhs.id)
            .append(this.active, rhs.active)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(id)
            .append(active)
            .toHashCode();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("id", id)
            .append("active", active)
            .toString();
    }
}
