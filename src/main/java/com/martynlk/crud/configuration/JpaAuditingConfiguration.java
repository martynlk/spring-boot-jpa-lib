package com.martynlk.crud.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import static com.martynlk.crud.configuration.JpaAuditingConfiguration.JPA_AUDITOR_BEAN_NAME;

@Configuration
@EnableJpaAuditing(auditorAwareRef = JPA_AUDITOR_BEAN_NAME)
public class JpaAuditingConfiguration {

    public static final String JPA_AUDITOR_BEAN_NAME = "auditorAware";
}