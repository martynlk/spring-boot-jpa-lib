package com.martynlk.crud.configuration;

import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public class CrudServiceConfiguration {

    @Min(1)
    private int createLimit;

    @Min(1)
    private int readLimit;

    @Min(1)
    private int updateLimit;

    @Min(1)
    private int deleteLimit;

    @NotNull
    private Sort.Direction defaultSortDirection;

    @NotEmpty
    @UniqueElements
    private List<@NotBlank String> defaultPropertiesToSortOn;

    public int getCreateLimit() {
        return createLimit;
    }

    public void setCreateLimit(int createLimit) {
        this.createLimit = createLimit;
    }

    public int getReadLimit() {
        return readLimit;
    }

    public void setReadLimit(int readLimit) {
        this.readLimit = readLimit;
    }

    public int getUpdateLimit() {
        return updateLimit;
    }

    public void setUpdateLimit(int updateLimit) {
        this.updateLimit = updateLimit;
    }

    public int getDeleteLimit() {
        return deleteLimit;
    }

    public void setDeleteLimit(int deleteLimit) {
        this.deleteLimit = deleteLimit;
    }

    public Sort.Direction getDefaultSortDirection() {
        return defaultSortDirection;
    }

    public void setDefaultSortDirection(Sort.Direction defaultSortDirection) {
        this.defaultSortDirection = defaultSortDirection;
    }

    public List<String> getDefaultPropertiesToSortOn() {
        return defaultPropertiesToSortOn;
    }

    public void setDefaultPropertiesToSortOn(List<String> defaultPropertiesToSortOn) {
        this.defaultPropertiesToSortOn = defaultPropertiesToSortOn;
    }
}
