package com.martynlk.crud.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:crudliquibase.properties")
public class CrudLiquibaseConfiguration {

    public static final String ID_COLUMN_NAME = "id_column_name";
    public static final String ID_COLUMN_TYPE = "id_column_type";
    public static final String ACTIVE_COLUMN_NAME = "active_column_name";
    public static final String ACTIVE_COLUMN_TYPE = "active_column_type";
    public static final String CREATED_BY_COLUMN_NAME = "created_by_column_name";
    public static final String CREATED_BY_COLUMN_TYPE = "created_by_column_type";
    public static final String CREATED_COLUMN_NAME = "created_column_name";
    public static final String CREATED_COLUMN_TYPE = "created_column_type";
    public static final String LAST_MODIFIED_COLUMN_NAME = "last_modified_column_name";
    public static final String LAST_MODIFIED_COLUMN_TYPE = "last_modified_column_type";
}
