package com.martynlk.crud.service.crudservice.valueretriever;

import com.martynlk.crud.configuration.CrudServiceConfiguration;
import com.martynlk.crud.message.BaseMessages;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Objects;

@Service
public class DefaultCrudServiceValueRetriever implements CrudServiceValueRetriever {

    private final CrudServiceConfiguration crudServiceConfiguration;

    public DefaultCrudServiceValueRetriever(
        CrudServiceConfiguration crudServiceConfiguration
    ) {
        this.crudServiceConfiguration = crudServiceConfiguration;
    }

    @Override
    public int getCreateLimit(){
        return crudServiceConfiguration.getCreateLimit();
    }

    @Override
    public int getReadLimit(Integer limitRequested, BaseMessages baseMessages){
        int readLimit = crudServiceConfiguration.getReadLimit();
        int limitToUse = Objects.isNull(limitRequested) ? readLimit : limitRequested;

        if(limitToUse > readLimit){
            throw new InvalidParameterException(baseMessages.readLimitExceeded(readLimit, limitToUse));
        }

        return limitToUse;
    }

    @Override
    public int getPage(Integer pageRequested){
        return Objects.isNull(pageRequested) ? 1 : pageRequested;
    }

    @Override
    public Sort.Direction getSortDirectionToUse(String sortDirectionRequested){
        return StringUtils.isBlank(sortDirectionRequested) ?
            crudServiceConfiguration.getDefaultSortDirection() :
            Sort.Direction.fromString(sortDirectionRequested)
        ;
    }

    @Override
    public List<String> getPropertiesToSortOn(List<String> propertiesRequestedToSortOn) {
        List<String> propertiesToSortOn = CollectionUtils.isEmpty(propertiesRequestedToSortOn) ?
            crudServiceConfiguration.getDefaultPropertiesToSortOn() :
            propertiesRequestedToSortOn
        ;

        return propertiesToSortOn;
    }

    @Override
    public int getUpdateLimit() {
        return crudServiceConfiguration.getUpdateLimit();
    }

    @Override
    public int getDeleteLimit() {
        return crudServiceConfiguration.getDeleteLimit();
    }
}
