package com.martynlk.crud.service.crudservice;

import com.martynlk.crud.configuration.CrudServiceConfiguration;
import com.martynlk.crud.dto.InputDto;
import com.martynlk.crud.dto.OutputDto;
import com.martynlk.crud.entity.BaseEntity;
import com.martynlk.crud.mapper.BaseMapper;
import com.martynlk.crud.message.BaseMessages;
import com.martynlk.crud.service.crudservice.valueretriever.CrudServiceValueRetriever;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
public class BaseCrudService<
    T extends InputDto,
    U extends BaseEntity,
    V extends OutputDto
> implements CrudService<T, V> {

    private final JpaRepository<U, UUID> jpaRepository;
    private final BaseMapper<T, U, V> baseServiceMapper;
    private final BaseMessages baseMessages;
    private final CrudServiceValueRetriever crudServiceValueRetriever;

    public BaseCrudService(
        JpaRepository<U, UUID> jpaRepository,
        BaseMapper<T, U, V> baseServiceMapper,
        BaseMessages baseMessages,
        CrudServiceValueRetriever crudServiceValueRetriever
    ) {
        this.jpaRepository = jpaRepository;
        this.baseServiceMapper = baseServiceMapper;
        this.baseMessages = baseMessages;
        this.crudServiceValueRetriever = crudServiceValueRetriever;
    }

    /**
     * Performs a batch save using by passing the {@code inputDtos}
     * specified to {@link JpaRepository#saveAll(Iterable)}.
     * <br/>
     * <br/>
     * If the {@code inputDtos} passed specify any fields that should be
     * set by the datasource when their {@link javax.persistence.Entity}
     * versions are saved, these fields will be nullified so that the
     * datasource is responsible for setting them.
     *
     * @param inputDtos
     *
     * @return The result of invoking {@link BaseMapper#entityToOutputDto(BaseEntity)}
     * on each record created.
     *
     * @throws InvalidParameterException if the number of elements in {@code inputDtos} is greater than the result of
     * invoking {@link CrudServiceConfiguration#getCreateLimit()} using the {@link CrudServiceConfiguration} passed as
     * a parameter to this {@link BaseCrudService}'s constructor.
     */
    @Override
    public Set<V> create(Set<T> inputDtos) {

        int createLimit = crudServiceValueRetriever.getCreateLimit();
        if(inputDtos.size() > createLimit){
            throw new InvalidParameterException(baseMessages.createLimitExceeded(createLimit, inputDtos.size()));
        }

        return inputDtos
            .stream()
            .map(baseServiceMapper::inputDtoToEntity)
            .map((baseEntity) -> {
                baseEntity.setCreatedBy(null);
                baseEntity.setCreated(null);
                baseEntity.setLastModified(null);
                return baseEntity;
            })
            .collect(Collectors.collectingAndThen(
                Collectors.toSet(),
                jpaRepository::saveAll
            ))
            .stream()
            .map(baseServiceMapper::entityToOutputDto)
            .collect(Collectors.toSet())
        ;
    }

    /**
     *
     * @param requestedLimit If this is set to 10, and the number of entries in the datasource this CRUD service interacts with
     *              is 11, the results returned will not include the 11th entry . If this is set to {@code null}, the
     *              limit used will be equal to {@link CrudServiceConfiguration#getReadLimit()}.
     *
     * @param requestedPage Used for pagination <b>iff</b> if a value for {@code limit} is also specified.
     *             <br/>
     *             <br/>
     *             <i>EXAMPLE:</i> If there are 100 entries in the datasource that this CRUD service interacts with, and
     *             the <code>limit</code> is 20, the total dataset will be paginated into 5 pages, since 100/20 = 5.
     *             If {@code page} is set to 1, the second page will be retrieved (pages are 0-indexed), so entries
     *             21 to 40 will be returned.
     *
     * @param requestedSortDirection Can be blank (null, empty, or pure whitespace), or a {@link Sort.Direction#name()}. If this
     *                      is blank, the value used will be equal to {@link CrudServiceConfiguration#getDefaultSortDirection()}.
     *
     * @param requestedPropertiesToSortOn Can be null or empty, or the names of fields on the {@link BaseEntity} sub-class
     *                           defined as a type when sub-classing this class. If this is null or empty, the value
     *                           used will be equal to {@link CrudServiceConfiguration#getDefaultPropertiesToSortOn()}.
     *
     * @return All data for the relevant resource in the datasource that this {@link BaseCrudService} interacts with,
     * limited and/or not paginated, as described above.
     * <br/>
     * <br/>
     * If there are no records in the datasource, an empty {@link Set} will be returned.
     *
     * @throws InvalidParameterException if {@code limit} is specified but is greater than the result of invoking
     * {@link CrudServiceConfiguration#getReadLimit()} using the {@link CrudServiceConfiguration} passed as a parameter
     * to this {@link BaseCrudService}'s constructor.
     */
    @Override
    public Set<V> getAll(Integer requestedLimit, Integer requestedPage, String requestedSortDirection, List<String> requestedPropertiesToSortOn) {
        List<String> propertiesToSortOn = crudServiceValueRetriever.getPropertiesToSortOn(requestedPropertiesToSortOn);

        PageRequest pageRequest = PageRequest.of(
            crudServiceValueRetriever.getPage(requestedPage),
            crudServiceValueRetriever.getReadLimit(requestedLimit, baseMessages),
            crudServiceValueRetriever.getSortDirectionToUse(requestedSortDirection),
            propertiesToSortOn.toArray(new String[propertiesToSortOn.size()])
        );

        return jpaRepository
            .findAll(pageRequest)
            .getContent()
            .stream()
            .map(baseServiceMapper::entityToOutputDto)
            .collect(Collectors.toSet())
        ;
    }

    /**
     *
     * @param id
     *
     * @return The result of invoking {@link BaseMapper#entityToOutputDto(BaseEntity)}
     * on the result of passing {@code id} to {@link JpaRepository#findById(Object)}.
     * Note that the result may not be active (may have been soft deleted). Therefore,
     * ensure that you handle such return values appropriately by overriding this method,
     * if necessary.
     */
    @Override
    public Optional<V> getOne(String id) {
        return jpaRepository
            .findById(UUID.fromString(id))
            .map(baseServiceMapper::entityToOutputDto)
        ;
    }

    /**
     * Performs a batch update by passing the result of {@link
     * JpaRepository#findAllById(Iterable)} to {@link
     * JpaRepository#saveAll(Iterable)} after invoking
     * {@link InputDto#getId()} on each of the {@code
     * inputDtos} passed.
     *
     * The result of invoking {@link BaseMapper#inputDtoToEntity(InputDto)}
     * on each of the {@code inputDtos} specified is used to perform the update.
     * <b>NOTE:</b> the update performed is dependent on the
     * {@link BaseMapper#inputDtoToEntity(InputDto)}s
     * {@link BaseEntity#update(Object)} implementation.
     * </p>
     * The following fields will be updated by this method, regardless of the
     * {@link BaseEntity#update(Object)} implementation:
     *
     * <ul>
     *     <li>Active</li>
     * </ul>
     *
     * @param inputDtos
     *
     * @return The result of invoking {@link BaseMapper#entityToOutputDto(BaseEntity)}
     * on each record updated.
     *
     * @throws EntityNotFoundException if any of the {@code inputDtos}
     * do not map to a record in the datasource.
     *
     * @throws InvalidParameterException if the number of elements in {@code inputDtos} is greater than the result of
     * invoking {@link CrudServiceConfiguration#getUpdateLimit()} using the {@link CrudServiceConfiguration} passed as
     * a parameter to this {@link BaseCrudService}'s constructor.
     */
    @Override
    public Set<V> update(Set<T> inputDtos){

        int updateLimit = crudServiceValueRetriever.getUpdateLimit();
        if(inputDtos.size() > updateLimit){
            throw new InvalidParameterException(baseMessages.updateLimitExceeded(updateLimit, inputDtos.size()));
        }

        List<U> entitiesToUpdate = inputDtos
            .stream()
            .map(T::getId)
            .collect(Collectors.collectingAndThen(
                Collectors.toSet(),
                jpaRepository::findAllById
            ))
            ;

        if(inputDtos.size() != entitiesToUpdate.size()){
            Collection<String> idsNotMappedToEntities = CollectionUtils.subtract(
                inputDtos.stream().map(InputDto::getId).collect(Collectors.toSet()),
                entitiesToUpdate.stream().map(U::getId).collect(Collectors.toSet())
            );

            String errorMessage = baseMessages.noEntityFound(new HashSet<>(idsNotMappedToEntities));
            throw new EntityNotFoundException(errorMessage);
        }

        List<U> entitiesWithUpdates = inputDtos
            .stream()
            .map(baseServiceMapper::inputDtoToEntity)
            .collect(Collectors.toList())
            ;

        entitiesToUpdate.sort(Comparator.comparing(U::getId));
        entitiesWithUpdates.sort(Comparator.comparing(U::getId));

        List<U> updatedEntities = new ArrayList<>();
        for(int i = 0; i < entitiesToUpdate.size(); i++){
            U entityToUpdate = entitiesToUpdate.get(i);
            U entityWithUpdate = entitiesWithUpdates.get(i);

            LocalDateTime originalCreated = entityToUpdate.getCreated();
            LocalDateTime originalLastModified = entityToUpdate.getLastModified();
            String originalCreatedBy = entityToUpdate.getCreatedBy();

            entityToUpdate.update(entityWithUpdate);

            entityToUpdate.setActive(entityWithUpdate.isActive());
            entityToUpdate.setCreated(originalCreated);
            entityToUpdate.setLastModified(originalLastModified);
            entityToUpdate.setCreatedBy(originalCreatedBy);


            updatedEntities.add(entityToUpdate);
        }

        return jpaRepository
            .saveAll(updatedEntities)
            .stream()
            .map(baseServiceMapper::entityToOutputDto)
            .collect(Collectors.toSet())
            ;
    }

    /**
     * Performs a hard batch delete of all {@link BaseEntity} instances
     * from the datasource whose {@link BaseEntity#getId()} is in the {@code ids}
     * passed and whose {@code active} field is set to {@code false}. If any of the
     * {@code id}s passed correspond to records in the datasource whose {@code active}
     * field is set to {@code true}, no records are deleted.
     *
     * @param ids
     *
     * @return The result of invoking {@link BaseMapper#entityToOutputDto(BaseEntity)}
     * on each record deleted.
     *
     * @throws InvalidParameterException if the number of elements in {@code inputDtos} is greater than the result of
     * invoking {@link CrudServiceConfiguration#getDeleteLimit()} using the {@link CrudServiceConfiguration} passed as
     * a parameter to this {@link BaseCrudService}'s constructor.
     *
     * @throws IllegalArgumentException if any of the {@code id}s passed correspond to
     * records in the datasource whose {@code active} field is set to {@code true}.
     */
    @Override
    public Set<V> delete(Set<String> ids){

        int deleteLimit = crudServiceValueRetriever.getDeleteLimit();
        if(ids.size() > deleteLimit){
            throw new InvalidParameterException(baseMessages.deleteLimitExceeded(deleteLimit, ids.size()));
        }

        Set<UUID> uuidsToDelete = ids
            .stream()
            .map(UUID::fromString)
            .collect(Collectors.toSet())
        ;

        Set<U> inactiveEntitiesFound = jpaRepository
            .findAllById(uuidsToDelete)
            .stream()
            .filter((entityToDelete) -> entityToDelete.isActive() == false)
            .collect(Collectors.toSet())
        ;

        if(inactiveEntitiesFound.size() != ids.size()){
            Collection<String> activeEntityIds = CollectionUtils.subtract(
                ids,
                inactiveEntitiesFound.stream().map(U::getId).collect(Collectors.toSet())
            );

            String errorMessage = baseMessages.entitiesToDeleteAreActive(new HashSet<>(activeEntityIds));
            throw new IllegalArgumentException(errorMessage);
        }

        jpaRepository.deleteInBatch(inactiveEntitiesFound);

        return inactiveEntitiesFound
            .stream()
            .map(baseServiceMapper::entityToOutputDto)
            .collect(Collectors.toSet())
            ;
    }
}
