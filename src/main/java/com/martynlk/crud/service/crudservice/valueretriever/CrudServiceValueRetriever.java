package com.martynlk.crud.service.crudservice.valueretriever;

import com.martynlk.crud.message.BaseMessages;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface CrudServiceValueRetriever {

    @Min(1) int getCreateLimit();

    @Min(1) int getReadLimit(Integer limitRequested, @NotNull BaseMessages baseMessages);

    @Min(1) int getPage(Integer pageRequested);

    @NotNull Sort.Direction getSortDirectionToUse(String sortDirectionRequested);

    @NotEmpty @UniqueElements List<@NotBlank String> getPropertiesToSortOn(@UniqueElements List<@NotBlank String> propertiesRequestedToSortOn);

    @Min(1) int getUpdateLimit();

    @Min(1) int getDeleteLimit();
}
