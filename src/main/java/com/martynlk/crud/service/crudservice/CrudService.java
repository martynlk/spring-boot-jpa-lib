package com.martynlk.crud.service.crudservice;

import com.martynlk.crud.annotation.group.Create;
import com.martynlk.crud.annotation.group.Update;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Validated
public interface CrudService<T, V> {

    /**
     * Should save the {@code inputDtos} specified.
     *
     * @param inputDtos
     *
     * @return The result of saving the {@code inputDtos} specified.
     *
     */
    @Validated({Default.class, Create.class})
    @NotEmpty Set<@NotNull @Valid V> create(@NotEmpty Set<@NotNull @Valid T> inputDtos);

    /**
     * Should return all data for the relevant resource in a datasource, limited and/or not paginated, as described
     * above.
     *
     * @param requestedLimit The number of data points to return.
     *
     * @param requestedPage Should be used for pagination to specify what page of data points to return.
     *
     * @param requestedSortDirection The sorting that should be applied to the result set after retrieval.
     *
     * @param requestedPropertiesToSortOn The properties that should be used when sorting the result set. This should
     *                                    contain field names equal to those defined in
     *                                    {@link com.martynlk.crud.dto.OutputDto} or a sub-class thereof.
     *
     * @return If there is no data for the relevant resource in a datasource, an empty {@link Set} should be returned.
     */
    @NotNull Set<@NotNull @Valid V> getAll(
        @Min(1) Integer requestedLimit,
        @Min(1) Integer requestedPage,
        String requestedSortDirection,
        @UniqueElements List<@NotBlank String> requestedPropertiesToSortOn
    );

    /**
     * Should return the data in the relevant resource for a datasource whose identifier matched the {@code id}
     * specified.
     *
     * @param id
     *
     * @return {@code null} if the {@code id} specified did not match any data in the datasource,
     * or a non-null {@code V} if it did ({@code V} should be the data that matched).
     * <br/>
     * <br/>
     * A {@code null} can be returned since, in some cases, a consumer of this library may just make use of this
     * class and they may want to react to data not being found.
     */
    @NotNull Optional<@Valid V> getOne(@NotBlank String id);

    /**
     * Should update the {@code inputDtos} specified for the relevant resource in a datsource.
     *
     * @param inputDtos
     *
     * @return The updated {@code inputDtos}.
     */
    @Validated({Default.class, Update.class})
    @NotEmpty Set<@NotNull @Valid V> update(@NotEmpty Set<@NotNull @Valid T> inputDtos);

    /**
     * Should remove the data in the relevant resource for a datasource whose identifiers match those {@code ids}
     * specified.
     *
     * @param ids
     *
     * @return The deleted {@code inputDtos}.
     */
    @NotEmpty Set<@NotNull @Valid V> delete(@NotEmpty Set<@NotBlank String> ids);
}
