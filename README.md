# Setting Up

## Entities

To declare an entity using this library, create a new class that extends the `BaseEntity` 
class from this library and ensure that it is annotated at the class-level with 
[@Entity](https://docs.oracle.com/javaee/7/api/javax/persistence/Entity.html). Finally,
ensure that the entity class extends the `BaseEntity` class from this library.

For each of these entities, ensure that you create the following columns in the
tables of the datasource that store the entities which extend `BaseEntity`.

| Column Name   | Datatype                                                                                 |
| ------------  | ---------------------------------------------------------------------------------------- |
| id            | [UUID](https://docs.oracle.com/javase/10/docs/api/java/util/UUID.html)                   |
| active        | [Boolean](https://docs.oracle.com/javase/10/docs/api/index.html?java/lang/Boolean.html)  |
| created_by    | [String](https://docs.oracle.com/javase/10/docs/api/index.html?java/lang/String.html)    |
| created       | [LocalDateTime](https://docs.oracle.com/javase/10/docs/api/java/time/LocalDateTime.html) |
| last_modified | [LocalDateTime](https://docs.oracle.com/javase/10/docs/api/java/time/LocalDateTime.html) |

Therefore, each entity you create should look like the following. If your entity has
other fields than those above, simply declare these as you would. In the following 
example, the entity also has a `name` column with type `varchar(255)`. 

**Note** that you will need to override the `update` method of `BaseEntity` but, to do
so, you will need to cast the `Object` passed to `update` to the `BaseEntity` sub-class
before you can do anything useful with it (consult the method's Javadoc for more info):

```
@Entity
public class DemoEntity extends BaseEntity {

    @NotBlank
    @Size(max = 255)
    private String name;

    public DemoEntity() {}

    public DemoEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void update(Object entityWithUpdates) {
        DemoEntity entityWithUpdatesAfterCast = (DemoEntity) entityWithUpdates;
        this.setName(entityWithUpdatesAfterCast.getName());
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof DemoEntity)) {
            return false;
        }
        DemoEntity castOther = (DemoEntity) other;
        return new EqualsBuilder()
            .appendSuper(super.equals())
            .isEquals()
        ;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .appendSuper(super.hashCode())
            .toHashCode()
        ;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .appendSuper(super.toString())
            .append("name", name)
            .toString()
        ;
    }
}
```

### Liquibase Provisions

This library provides you with [Liquibase](http://www.liquibase.org/index.html) 
changelog parameters to help manage your database migrations. Liquibase is 
named as a supported dependency at [https://start.spring.io/]() and is a good 
choice for this job! You can find instructions on how to set-up Liquibase
[here](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html).

The following changelog parameters are defined by this library for use in your 
application's Liquibase changelogs (if you decide to use Liquibase for managing
database migrations):

- id_column_name
- id_column_type
- active_column_name
- active_column_type
- created_by_column_name
- created_by_column_type
- created_column_name
- created_column_type
- last_modified_column_name
- last_modified_column_type

The snippet below can then be placed in your changelog file. By default, this should 
be located in `src/main/resources/db/changelog/db.changelog-master.yaml`:

```
databaseChangeLog:
- changeSet:
    id: your_id_value_here
    author: your_name_here
    changes:
    - createTable:
        tableName: your_entity_name
        columns:
        - column:
            name: ${id_column_name}
            type: ${id_column_type}
            constraints:
              primaryKey: true
              nullable: false
        - column:
            name: ${active_column_name}
            type: ${active_column_type}
            constraints:
              nullable: false
        - column:
            name: ${created_by_column_name}
            type: ${created_by_column_type}
            constraints:
              nullable: false
        - column:
            name: ${created_column_name}
            type: ${created_column_type}
            constraints:
              nullable: false
        - column:
            name: ${last_modified_column_name}
            type: ${last_modified_column_type}
            constraints:
              nullable: false
```

If you want to override any of the values defined for these changelog parameters, simply
override those required in your `application.yml` or `application.properties`. Note that, 
if you override any of the parameters ending in `_name`, you will need to redeclare the 
relevant `@Column` annotation on the field in your `@Entity` classes that sub-classes 
`BaseEntity`. Furthermore, if you override any of the parameters ending in `_type`, you 
will need to redeclare the type of the relevant field in your `@Entity` classes that 
sub-classes `BaseEntity`. So, if you redefine the following changelog parameters like 
so:

```
spring.liquibase.parameters.id_column_name=My Custom ID Column Value
spring.liquibase.parameters.id_column_type=BIGINT
```

You will need to redefine your `@Entity` classes that sub-class `BaseEntity` so that: 

1. The `id` field in the relevant entity has a `name` attribute for the `@Column` 
annotation.

2. The type of the `id` field in the relevant entity is modified so that it maps to
the database data type defined.

In other words:

```
@Column(name="My Custom ID Column Value")
private Long id;
```

## Repositories

Ensure that you include the [@EnableJpaRepositories](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/config/EnableJpaRepositories.html)
annotation somewhere in your application (usually at the class-level of a class called 
`JpaConfiguration` or something similar), and define where the repositories in your 
application are located.

Then, for each repository that accesses a table mapped to your applications'
`@Entity`s, create an interface that extends [JpaRepository](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html) 
and annotate it at the class-level with 
[@Repository](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/stereotype/Repository.html).

If your applications' `@Entity`s extend this library's `BaseEntity` ensure that the second
generic type of the `JpaRepository` is `UUID` since the `@Id` field of `BaseEntity` is 
of the `UUID` data type.

For the `DemoEntity` example above, its corresponding repository would be
as follows:

```
@Repository
public interface DemoEntityRepository extends BaseJpaRepository<DemoEntity, UUID> { }
```

## Service

To create your own CRUD services, you can extend the `BaseCrudService` class found 
in this library, and use the `CrudService` interface when auto-wiring these 
CRUD services in your application.

If you do extend your CRUD services from `BaseCrudService`, you will need to provide
four dependencies:

- A [JpaRepository](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html)
- A class that extends the `BaseMapper` class from this library
- A class that extends the `BaseMessages` class from this library
- A class that extends the `CrudServiceConfiguration` class from this library